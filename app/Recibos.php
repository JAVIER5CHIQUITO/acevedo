<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recibos extends Model
{
    //
     protected $table='recibos';
    
    protected $fillable = ['nomina_id','contrato_id','monto'];
}
