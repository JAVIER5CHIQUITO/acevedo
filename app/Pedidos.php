<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    //
     protected $table='pedidos_ventas';
    
    protected $fillable = ['empresa_id','cliente_id','fecha','hora','num_doc','total','status'];

    public function empresa(){
    	return $this->belongsTo( Empresa::class );
    }

	public function lineas(){
    	return $this->hasMany( Lineas_Pedido::class, 'pedido_venta_id', 'id');
    }

    public function cliente(){
    	return $this->belongsTo( Entidades::class );
    }
}
