<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
     protected $table='empresas';
    
    protected $fillable = ['entidad_id','nombre'];

	public function articulos(){
    	return $this->hasMany( Productos::class, 'empresa_id', 'id');
    }

	public function pedidos(){
    	return $this->hasMany( Pedidos::class, 'empresa_id', 'id');
    }

	public function facturas_compras(){
    	return $this->hasMany( Compras::class, 'empresa_id', 'id');
    }

	public function facturas_ventas(){
    	return $this->hasMany( Ventas::class, 'empresa_id', 'id');
    }
}
