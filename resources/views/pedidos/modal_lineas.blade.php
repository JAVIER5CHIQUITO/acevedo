    <!-- Modal-->
    <div class="modal fade bs-example-modal-lg" id="modal_linea" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Linea de Pedido</h4>                 
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="POST" id="form_linea_pedido" action="{{url('Nueva_Linea_Pedido')}}">              
              {{ csrf_field() }}
              <div class="form-group" id="articulos"> 
                <label for="ex3">Artículo</label>
                <select class='form-control' onchange="cargar_precio(this);" id='articulo_id' name='articulo_id' required='required' style='max-width: 723px;' class='form-control' title='Artículo'>
                  <option value=''>Seleccione..</option>
                </select>
              </div>
              <div class="form-inline"> 
                <label for="ex3">Cantidad</label>
                <input type="number" min="1" value="1" onchange="calcular_importe(this);" onkeyup="calcular_importe(this);" id="cantidad" name="cantidad" required="required" class="form-control" style="width: 150px;" placeholder="Cantidad">
                <label for="ex3">Precio</label>
                <input type="number" min="0" id="precio" name="precio" disabled="disabled" class="form-control" style="width: 200px;" placeholder="Precio">
                <label for="ex3">Importe</label>
                <input type="number" min="0" id="importe" name="importe" disabled="disabled" class="form-control" style="width: 200px;" placeholder="Importe">
              </div>
              <div class="hidden">
                <button type="reset" id="btn_reset" class="btn btn-default btn-xs hidden">Reset</button>
                <input type="hidden" name="pedido_id" id="pedido_id" value="{{$pedido->id}}">
                <input type="hidden" name="linea_id" id="linea_id" class="hidden">                
              </div>
          </div>
              <div class="modal-footer">
                <button type="button" id="btnCloseModalSet" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                <button type="submit" id="btnAceptModalSet" class="btn btn-primary">Guardar</button>
              </div>
            </form>
        </div>
      </div>
    </div>
    <!-- /Modal -->