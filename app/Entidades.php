<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidades extends Model
{
    //
    protected $table='entidades';
    
    protected $fillable = ['nombre','apellido','cedula','email','foto','estatus','es_proveedor','es_cliente','es_empleado'];
}
