<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Compras;
use App\Productos;
use App\Movimientos;
use App\Entidades;

class ComprasController extends Controller
{
    //
    public function index()
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		if ( !is_null($empresa) ) {
			$facturas_compras = $empresa->facturas_compras->where('es_ajuste','=', 0);
			return view('compras.compras', compact('facturas_compras'));
		}else {
			return view('compras.compras');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$compra = Compras::find($id);
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$articulos = $empresa->articulos;
		$proveedores = Entidades::where('es_proveedor','=',1)->get();
		return view('Compras.formCompra', compact('articulos', 'proveedores', 'compra'));
	}

	public function show_form()
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$articulos = $empresa->articulos;
		$proveedores = Entidades::where('es_proveedor','=',1)->get();
		return view('compras.formCompra', compact('articulos', 'proveedores'));
	}

	public function store(Request $request)
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$proveedor = Entidades::find($request['proveedor_id']);

		if ( (!is_null($empresa)) && (!is_null($proveedor)) ) {   

			$ult_compra = $empresa->facturas_compras->where('es_ajuste','=', 0)->last();
			if ( is_null( $ult_compra ) ) {
				$num_doc = "FC001";
			} else {
				$num_doc = substr( $ult_compra->num_doc, 4, strlen( $ult_compra->num_doc )) + 1;
				$num_doc = "FC00".$num_doc;

			}

			$compra = new Compras([
				'empresa_id'=>$empresa->id,
				'creador_id'=>\Auth::user()->entidad_id,
				'proveedor_id'=>$proveedor->id,
				'num_doc'=>$num_doc,
				'num_fac_prv'=>$request['num_fac_prv'],
				'fecha'=>$request['fecha'],
				'hora'=>$request['hora']
			]);  
			$compra->save(); 
			$mensaje = "Registro creado con éxito, incluye los articulos a comprar";            
			\Session::flash('msgExito', $mensaje );
			return redirect('/Compra/Ver/'.$compra->id);
		}else {
			$mensaje = "Error al ubicar la ficha de la empresa o el proveedor seleccionado";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}

	public function update(Request $request)
	{
		$proveedor = Entidades::find($request['proveedor_id']);
		$compra = Compras::find($request['id']);
		if ( (!is_null($proveedor)) && (!is_null($compra) ) ) {  
			$compra->proveedor_id = $proveedor->id;
			$compra->fecha = $request['fecha'];
			$compra->hora = $request['hora'];
			$compra->save(); 
			$mensaje = "Registro actualizado con éxito, incluye los articulos a comprar";            
			\Session::flash('msgExito', $mensaje );
			return back();
		}else {
			$mensaje = "Error al ubicar la ficha del proveedor o la compra";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}
	public function store_lin(Request $request)
	{
		$compra = Compras::find($request['compra_id']);
		$articulo = Productos::find($request['articulo_id']);
		if ( (!is_null($compra)) && (!is_null($articulo)) ) { 
			$can = $request['cantidad'];
			if ( $can < 1 ) {
				$can = 1;
			}
			$pre = $request['precio'];
			if ( $pre < 1 ) {
				$pre = 1;
			}
			$imp = $can * $pre;
			$linea_compra = Movimientos::where([
									['articulo_id','=',$articulo->id],
									['factura_compra_id', '=', $compra->id]
								])->first();
			if ( is_null($linea_compra)) {
				$linea_compra = new Movimientos([
					'articulo_id'=>$articulo->id,
					'factura_compra_id'=>$compra->id,
					'fecha'=>$compra->fecha,
					'hora'=>$compra->hora,
					'cantidad'=>$can,
					'precio'=>$pre,
					'importe'=>$imp
				]);  
				$linea_compra->save();            	
			}else{
				$can = $linea_compra->cantidad + $can;
				$imp = $can * $pre;
				$linea_compra->cantidad = $can;
				$linea_compra->importe = $imp;
				$linea_compra->save();
			}
			$mensaje = "Registro creado con éxito";            
			\Session::flash('msgExito', $mensaje );            
			return back();
		}else {
			$mensaje = "Error al ubicar el documento o el artículo seleccionado";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}

	public function update_lin(Request $request)
	{
		$compra = Compras::find($request['compra_id']);
		$linea_compra = Movimientos::where([
									['factura_compra_id', '=', $compra->id],
									['id', '=', $request['linea_id']]
								])->first();
		 $articulo = Productos::find($request['articulo_id']);
		if ( (!is_null($linea_compra)) && (!is_null($compra) ) && (!is_null($articulo) ) ) {  
			$can = $request['cantidad'];
			if ( $can < 1 ) {
				$can = 1;
			}
			$pre = $request['precio'];
			if ( $pre < 1 ) {
				$pre = 1;
			}
			$imp = $can * $pre;
			$linea_compra->articulo_id = $articulo->id;
			$linea_compra->cantidad = $can;
			$linea_compra->precio = $pre;
			$linea_compra->importe = $imp;
			$linea_compra->save();

			$mensaje = "Registro actualizado con éxito";            
			\Session::flash('msgExito', $mensaje );
			return back();			
		}else {
			$mensaje = "Error al ubicar la ficha de la empresa o el documento";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}

	public function destroy_lin(Request $request)
	{
		$compra = Compras::find($request['id']);
		$linea_compra = Movimientos::where([
									['factura_compra_id', '=', $compra->id],
									['id', '=', $request['linea_id']]
								])->first();
		if ( (!is_null($linea_compra)) && (!is_null($compra) ) ) {  
			$linea_compra->delete();
		}
	}

	public function load_lin(Request $request)
	{
		$compra = Compras::find($request['id']);
		$tabla  = "<table class='table table-bordered' id='movimientos'>
					<thead>
					  <tr>
						<th style='text-align:center; width: 10px'><input type='checkbox' id='checkAllLineas' onchange='selectAllLineas(this);'></th>
						<th style='width: 10px'>#</th>
						<th style='text-align:center; width: 160px;'>Articulo</th>
						<th style='text-align:center; width: 100px;'>Existencia</th>
						<th style='text-align:center; width: 100px;'>Cantidad</th>
						<th style='text-align:center; width: 120px;'>Precio</th>
						<th style='text-align:center; width: 120px;'>Importe</th>
					  </tr> 
					</thead> 
					<tbody>";
		if ( !is_null($compra) )  {
		  $movimientos = $compra->movimientos;
		  if ( sizeof( $movimientos ) > 0 ){
			$cont = 0;
			foreach ($movimientos as $item):
			  $cont = $cont + 1;
			  if($item->articulo->existencia < $item->cantidad) { $style="text-align:center; color: red;"; }else{ $style="text-align:center;"; }
			  $tabla .="<tr>
						  <th style='text-align:center;'>
							<input type='checkbox' value='".$item->id."*".$item->articulo_id."*".$item->cantidad."*".$item->precio."*".$item->importe."'>
						  </th>
						  <td>".$cont."</td>
						  <td>".$item->articulo->nombre."</td>
						  <td style='".$style."'>".$item->articulo->existencia."</td>
						  <td>".$item->cantidad."</td>
						  <td>".$item->precio."</td>
						  <td>".$item->importe."</td>
						</tr>";                                               
			endforeach;
		  }
		}        
		$tabla .= " </tbody>
				  </table>";
		return $tabla;
	}

	public function destroy($id)
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$compra = Compras::where([
									['id', '=', $id],
									['empresa_id', '=', $empresa->id]
								])->first();
		if ( !is_null($compra) ) {
			Movimientos::where('factura_compra_id', '=', $compra->id)->delete();
			$compra->delete();
			$mensaje = "Registro eliminado con éxito";            
			\Session::flash('msgExito', $mensaje );
			return redirect('ver_compras');
		}else {
			$mensaje = "Error al ubicar la ficha de la compra";          
			\Session::flash('msgError', $mensaje );            
			return redirect('ver_compras');
		}
	}
}
