<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>{{ config('app.name', 'Acevedo|Embutidos') }}</title>

    <!-- Styles -->
   
    <link rel="stylesheet" type="text/css" href="{{asset('css/css.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/fileinput.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-table.css')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('imagenes/favicon-96x96.png')}}">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">

      @yield('content')

    </div>

    <footer>
      <p>© 2018<a style="color:#0a93a6; text-decoration:none;" href="#">Embutidos Acevedo.2018</a> Derechos Reservados Jch13</p>
    </footer>
    <!-- Scripts -->
    <script type="text/javascript" src="{{asset('js/jquery-3.1.1.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-table.js')}}"></script>
    <script type="text/javascript" src="{{asset('src/locale/bootstrap-table-es-CL.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
</html>
<script>
//$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['es-ES']);
</script>