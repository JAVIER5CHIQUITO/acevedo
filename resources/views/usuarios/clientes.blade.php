@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

 <div class="col-md-9">
 <div class="panel-group" id="accordion">
<!--Latest User-->
    <div class="panel panel-default">
      <div class="panel-heading panel-color" style="background-color:#095f59">
        <h3 class="panel-title">Lista Clientes</h3>
      </div>
      <div class="panel-body">
       <table data-toggle="table" data-toolbar="#toolbar" data-locale="es-ES" data-search="true" data-url="{{url('clientes')}}" data-show-refresh="true">

        <thead>
        <tr>
          <th data-field="nombre" >Nombre</th>
          <th data-field="cedula" >Cedula</th>
          <th data-field="correo" >Correo</th>
        </tr>
        </thead>
       
      </table>
    
      </div>
    </div>

      </div>
    </div>
  </div>
</section>
@endsection