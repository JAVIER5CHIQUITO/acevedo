<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Entidades;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|max:255',
            'cedula' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        
        $entidad = new Entidades();
        $entidad->nombre = $data['nombre'];
        $entidad->apellido = $data['apellido'];
        $entidad->cedula = $data['cedula'];
        $entidad->email = $data['email'];
         
         if($data['tipo'] == 1){
         //1 es empleado
          $entidad->es_empleado = 1;
             
         }
         
         if($data['tipo'] == 2){
         //1 es proveedor
          $entidad->es_proveedor = 1;
             
         }
         
         if($data['tipo'] == 3){
         //1 es cliente
          $entidad->es_cliente = 1;
             
         }
         
        $entidad->save();
        
        $usua= User::create([
           'entidad_id' => $entidad->id,
           'user' => $entidad->nombre.' '.$entidad->apellido,
           'email' => $entidad->email,
           'password' => bcrypt($data['password']),
        ]);


        \Session::flash('flash_message', 'usuario creado con exito!!! felicidades');
        return $usua;
        
    }
}
