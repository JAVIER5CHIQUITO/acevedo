<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Productos;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        if ( !is_null($empresa) ) {
            $articulos = $empresa->articulos;
            return view('productos.productos', compact('empresa', 'articulos'));
        }else {
            return view('productos.productos');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        if ( !is_null($empresa) ) {   
            $codigo = strtoupper($request['codigo']);
            $articulo = Productos::where([
                                    ['empresa_id', '=', $empresa->id],
                                    ['codigo', '=', $codigo]
                                ])->first();

            if ( is_null($articulo) ) {                 
                if ( is_null($request['es_especialidad']) ) {
                    $vBool = 0;
                } else {                        
                    $vBool = 1;
                }                 
                $articulo = new Productos([
                    'empresa_id'=>$empresa->id,
                    'codigo'=>strtoupper($request['codigo']),
                    'nombre'=>strtoupper($request['nombre']),
                    'es_especialidad'=>$vBool
                ]);  
                $articulo->save();    
                $mensaje = "Registro creado con éxito";            
                \Session::flash('msgExito', $mensaje );
                return back();
            }else {
                $mensaje = "Ya existe un Articulo registrado con ese código";          
                \Session::flash('msgError', $mensaje );
                return back();
            }   
        }else {
            $mensaje = "Error al ubicar la ficha de la empresa";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        if ( !is_null($empresa) ) { 
            $articulo = Productos::where([
                                    ['id', '=', $id],
                                    ['empresa_id', '=', $empresa->id]
                                ])->first();
            if ( !is_null($articulo) ) { 
                //$movimientos = $articulo->movimientos()->orderby(['fecha' => 'desc', 'hora' => 'desc']); 
                $movimientos = $articulo->movimientos->sortByDesc(['fecha', 'hora']); 
                //$movimientos = $articulo->movimientos;                                          
                
                return view('productos.formProducto', compact('empresa', 'articulo', 'movimientos'));
            }else {
                $mensaje = "Error al ubicar la ficha del Articulo";          
                \Session::flash('msgError', $mensaje );
                return back();
            }
        }else {
            $mensaje = "Error al ubicar la ficha de la empresa";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        if ( !is_null($empresa) ) { 
            $codigo = strtoupper($request['codigo']);
            $articulo = Productos::where([
                                    ['empresa_id', '=', $empresa->id],
                                    ['codigo', '=', $codigo]
                                ])->first();

            if ( ( is_null($articulo) ) || ( $articulo->id == $request['id'] ) ) {                 
                $articulo             = Productos::where([
                                            ['id', '=', $request['id']],
                                            ['empresa_id', '=', $empresa->id]
                                        ])->first();
                if ( !is_null($articulo) ) {
                    $articulo->empresa_id = $empresa->id;
                    $articulo->codigo     = strtoupper($request['codigo']);
                    $articulo->nombre     = strtoupper($request['nombre']);
                    if ( is_null($request['es_especialidad']) ) {
                        $vBool = 0;
                    } else {                        
                        $vBool = 1;
                    }                    
                    $articulo->es_especialidad = $vBool;
                    $articulo->save();  
                    $mensaje = "Registro actualizado con éxito";            
                    \Session::flash('msgExito', $mensaje );
                    return back();
                }else {
                    $mensaje = "Error al ubicar la ficha del Articulo";          
                    \Session::flash('msgError', $mensaje );            
                    return redirect('ver_productos');
                }
            }else {
                $mensaje = "Ya existe un Articulo registrado con ese código";          
                \Session::flash('msgError', $mensaje );
                return back();
            }
        }else {
            $mensaje = "Error al ubicar la ficha de la empresa";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function load(Request $request)
    {
        $empresa = Empresa::find($request['empresa_id']);
        if ( !is_null($empresa) ) { 
            $articulos = $empresa->articulos->where('es_especialidad','=', 1);
            $select = "<select class='form-control' id='articulo_id' name='articulo_id' onchange='cargar_precio(this);' required='required' style='max-width: 723px;' class='form-control' title='Artículo'>";
                        if (sizeof( $articulos ) > 0) {
                            $select .= "<option value=''>SELECCIONE...</option>";
                            foreach ($articulos as $item) {
                                $select .= "<option value='".$item->id."'>".$item->nombre."</option>";
                            }
                        } else {
                            $select .= "<option value=''>SELECCIONE...</option>";
                        }
            $select .= "</select>";
            return $select;
                
        }else {
            return "Error";
        }
    }

    public function load_precio(Request $request)
    {
        $articulo = Productos::find($request['articulo_id']);
        if ( !is_null($articulo) ) { 
            return $articulo->precio;                
        }else {
            return "Error";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        $articulo = Productos::where([
                                    ['id', '=', $id],
                                    ['empresa_id', '=', $empresa->id]
                                ])->first();
        if ( !is_null($articulo) ) {
            $articulo->delete();
            $mensaje = "Registro eliminado con éxito";            
            \Session::flash('msgExito', $mensaje );
            return redirect('ver_productos');
        }else {
            $mensaje = "Error al ubicar la ficha del Articulo";          
            \Session::flash('msgError', $mensaje );            
            return redirect('ver_productos');
        }
    }
}
