@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

 <div class="col-md-9">
 <div class="panel-group" id="accordion">
  <div class="panel panel-default overflow-hidden">
  <div class="panel-heading apnel-color" style="background-color:  #095f59; color: #ffffff;">
    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" style="color: #ffffff;" data-parent="#accordion" href="#form">
      <i class="icono glyphicon glyphicon-plus pull-right"></i> 
      <h3 class="panel-title"> 
         Registrar Producto
      </h3>
    </a>
  </div>
  <div id="form" class="panel-collapse collapse ">
  <div class="panel-body">
     <form action="{{url('Guardar_Articulo')}}" method="POST" data-parsley-validate="true">
				         {{ csrf_field() }}
		<!-- begin wizard step-1 -->
        <fieldset>

                                            <div class="corregir col-md-12" id="corregir"></div>
                                            <div class="row">    
                                              <legend>Datos Del Producto:</legend>                                          
                                                <!-- begin col-6 -->
                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                    <label class="control-label col-md-4">Empresa: </label>
                                                    <select name="empresa_id" id="empresa_id" class="form-control selectpicker" data-size="10" data-live-search="true" required="required">
                                                      @if ( isset($empresa) )
                                                        <option value="{{$empresa->id}}" selected> {{$empresa->nombre}} </option>   
                                                      @else
                                                        <option value="" disabled selected> No has actualizado la informacion de la Empresa</option>   
                                                      @endif                                                                         
                                                    </select>
                                                  </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                      <input type="checkbox" name="es_especialidad" id= es_especialidad" value='1'><b> Especialidad de la Empresa</b>
                                                    </div>
                                                </div>
                                                <br>
                                                <!-- end col-6 -->                                              
                                            </div>
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Código: </label>
                                                        <input type="text" id="codigo" name="codigo" class="form-control" required />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->

                                                <!-- begin col-4 -->
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Nombre: </label>
                                                        <input type="text" id="nombre" name="nombre" class="form-control" required />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>

                                       <!-- <div class="row">
                                              begin col-4 
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Existencia</label>
                                                        <input type="text" style="text-align: right;" id="existencia" name="existencia" class="form-control" disabled="disabled" />
                                                    </div>
                                                </div>-->
                                                <!-- end col-4 

                                            </div>-->
                                            <!-- end row -->
                                            <br>
                                            <!--begin row -->
                                            <div class="row">
                                              <div class="col-md-12 pull-right">
                                                <button  type="submit" class="pull-right btn-large btn btn-success"> Enviar</button>
                                              </div>
                                            </div>
                                </fieldset>
                            </form>
  </div>
  </div>
  </div>
</div>
<!--Latest User-->
<div class="panel-group" id="accordion">
 <div class="panel panel-default overflow-hidden">
  <div class="panel-heading panel-color" style="background-color:#095f59; color: #ffffff;">
    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" style="color: #ffffff;" data-parent="#accordion" href="#table" aria-expanded="true">
      <i class="icono glyphicon glyphicon-plus pull-right"></i> 
      <h3 class="panel-title"> Listado </h3>
    </a>
  </div>
  <div id="table" class="panel-collapse collapse in">
  <div class="panel-body">
    <table data-toggle="table" data-toolbar="#toolbar" data-locale="es-ES" data-search="true" data-show-refresh="true">
      <thead>
        <tr>
            <th style='text-align:center; width: 40px;'>#</th>
            <th style='text-align:center;'>Código</th>
            <th style='text-align:center;'>Nombre</th>
            <th style='text-align:center; width: 100px;'>Existencia</th>
            <th style='text-align:center; width: 100px;'>Precio</th>
            <th style='text-align:center;'>Acciones</th>
        </tr>
      </thead>
      <tbody>
        @if( isset( $articulos ) )
          @if( sizeof( $articulos) > 0 )
            <?php $cont = 0; ?>
            @foreach ($articulos as $item)
              <?php $cont = $cont + 1; ?>
              <tr>
                <td style='text-align:right;'>{{$cont}}</td>
                <td style='text-align:left;'>{{$item->codigo}}</td>
                <td style='text-align:left;'>{{$item->nombre}}</td> 
                <td style='text-align:left;'>{{$item->existencia}}</td>  
                <td style='text-align:left;'>{{$item->precio}}</td>  
                <td style='text-align:center;' class="reportHide">
                  <a href="{{url('/Articulos/Ver/'.$item->id)}}" title="Editar" class="btn btn-info btn-xs reportHide">
                    <i class="glyphicon glyphicon-pencil"></i>
                  </a>
                  <a href="{{url('/Articulos/Eliminar/'.$item->id)}}" title="Eliminar" class="btn btn-danger btn-xs reportHide" >
                    <i class="glyphicon glyphicon-trash"></i>
                  </a>
                </td>
              </tr>
            @endforeach
          @else 
            <tr>
              <td colspan="6">
                <div class="alert alert-info alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-info"></i> Aviso!</h4>
                  No hay ningun articulo registrado.
                </div>
              </td>
            </tr>          
          @endif
        @else
          <tr>
            <td colspan="6">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Aviso!</h4>
                No hay ningun articulo registrado.
              </div>
            </td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>
</div>
</div>

      </div>
    </div>
@endsection