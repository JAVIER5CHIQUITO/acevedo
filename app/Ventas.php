<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    //
    protected $table = 'facturas_ventas';
    
    protected $fillable = ['empresa_id','creador_id','cliente_id','fecha','hora','num_doc','es_ajuste','total'];

	public function movimientos(){
    	return $this->hasMany( Movimientos::class, 'factura_venta_id', 'id');
    }

    public function empresa(){
    	return $this->belongsTo( Empresa::class );
    }

    public function cliente(){
    	return $this->belongsTo( Entidades::class );
    }
}
