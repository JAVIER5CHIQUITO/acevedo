@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

 <div class="col-md-9">
 <div class="panel-group" id="accordion">
  <div class="panel panel-default overflow-hidden">
  <div class="panel-heading apnel-color" style="background-color:  #095f59;">
     <h3 class="panel-title">  <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="#form">
     <i class="icono glyphicon glyphicon-plus pull-right"></i> 
         Especialidades
    </a></h3>
  </div>
  <div id="form" class="panel-collapse collapse ">
  <div class="panel-body">
     <form action="" method="POST" data-parsley-validate="true">
				         {{ csrf_field() }}
		<!-- begin wizard step-1 -->
        <fieldset>

                                            <div class="corregir col-md-12" id="corregir"></div>
                                            <div class="row">
                                            <legend>Datos Del Producto:</legend>
                                                        <input type="hidden" id="id_chofer" name="id_chofer" />
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Nombre: </label>
                                                        <input type="text" id="nombre" name="nombre" class="form-control" required />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->

                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Apellido: </label>
                                                        <input type="text" id="apellido" name="apellido" class="form-control" />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Telefonos: </label>
                                                        <div class="inp-telefonos"></div>
                                                        <input type="text" id="telefono" name="telefono" class="form-control" />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>

                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Placa Auto</label>
                                                        <input type="text" id="placa" name="placa-auto" class="form-control" required />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->

                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Empresa</label>
                                                        <select name="id-empresa" class="form-control selectpicker" data-size="10" data-live-search="true"/>
                                                           <option disabled selected> seleccione....</option>
                                                           
                                                           <option value=""></option>
                                                        
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->

                                            </div>
                                            <!-- end row -->
                                            <!--begin row -->
                                            <div class="row">
                                                
                                               <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Deuda Credito</label>
                                                        <input type="text" name="deuda-credito" class="form-control" required />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->

                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Dias Atraso</label>
                                                        <input type="number" name="dias-atraso" placeholder="" min="0" class="form-control" required />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Monto Atraso</label>
                                                        <input type="text" name="monto-atrasado" placeholder="" class="form-control" required />
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->

                                            </div>
                                            <!-- end row -->
                                            <!--begin row -->
                                            <div class="row">
                                            
                                            <label class="control-label col-md-4">Motivos:</label>
                                                <div class="col-md-12" id="motivos">
                                                  <select name="motivo[]" class="form-control multiple-select2" multiple="multiple" required>
                								   
                                                     <option value="" ></option>
                                                    
                                                  </select>
                                                </div>
                                            <br><br><br>
                                            <br><br><br>
                                            <div class="col-md-12 pull-right">
                                            <button  type="submit" class="pull-right btn-large btn btn-success"> Enviar</button>
                                            </div>
                                </fieldset>
                            </form>
  </div>
  </div>
  </div>
</div>
<!--Latest User-->
<div class="panel-group" id="accordion">
 <div class="panel panel-default overflow-hidden">
  <div class="panel-heading panel-color" style="background-color:#095f59">
    <h3 class="panel-title"> <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="#table" aria-expanded="true">
     <i class="icono glyphicon glyphicon-plus pull-right"></i> Lista Especialidades</a> </h3>
  </div>
  <div id="table" class="panel-collapse collapse ">
  <div class="panel-body">
    <table data-toggle="table" data-toolbar="#toolbar" data-locale="es-ES" data-search="true" data-show-refresh="true">
      <caption class="text-center">Especialidades </caption>
    <thead>
    <tr>
        <th>Name</th>
        <th>Stars</th>
        <th>Forks</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
      <tr>
        <td>Condición</td>
        <td>Soleado</td>
        <td>Mayormente soleado</td>
        <td>Parcialmente nublado</td>
      </tr>
      <tr>
        <td>Temperatura</td>
        <td>19°C</td>
        <td>17°C</td>
        <td>12°C</td>
      </tr>
      <tr>
        <td>Vientos</td>
        <td>E 13 km/h</td>
        <td>E 11 km/h</td>
        <td>S 16 km/h</td>
      </tr>
    </tbody>
</table>
  </div>
</div>
</div>
</div>

      </div>
    </div>
@endsection