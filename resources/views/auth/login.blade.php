@extends('layouts.header')

@section('content')

@include('layouts.panel-inicio')

<div class="container">
	<form class="form-signin" role="form" method="POST" action="{{ url('/login') }}">
	                         {{csrf_field()}}
	   <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		<input type="email" class="form-control" name="email" placeholder="Correo" required>
	        @if ($errors->has('email'))
             <span class="help-block">
              <strong>{{$errors->first('email')}}</strong>
             </span>
            @endif
	   </div>
	   <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		<input type="password" class="form-control" name="password" placeholder="Contraseña" required>
	        @if($errors->has('password'))
              <span class="help-block">
                <strong>{{$errors->first('password')}}</strong>
              </span>
            @endif
	   </div>
		<button class="btn btn-inverse btn-block" type="submit">Iniciar</button>
	</form>
     <div class="col-md-offset-5">
      <h6 class="texto-color">	No tienes Cuenta <a href="{{url('register')}}" class="">Registrate</a> </h6>
     </div>
</div>
@endsection
