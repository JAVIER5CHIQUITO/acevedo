<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Ventas;
use App\Productos;
use App\Movimientos;
use App\Entidades;

class VentasController extends Controller
{
    //
        public function index()
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        if ( !is_null($empresa) ) {
            $facturas_ventas = $empresa->facturas_ventas->where('es_ajuste','=', 0);
            return view('ventas.ventas', compact('facturas_ventas'));
        }else {
            return view('ventas.ventas');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venta = Ventas::find($id);
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        $articulos = $empresa->articulos;
        $clientes = Entidades::where('es_cliente','=',1)->get();
        return view('ventas.formVenta', compact('articulos', 'clientes', 'venta'));
    }

    public function show_form()
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        $articulos = $empresa->articulos;
        $clientes = Entidades::where('es_cliente','=',1)->get();
        return view('ventas.formVenta', compact('articulos', 'clientes'));
    }

    public function store(Request $request)
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        $cliente = Entidades::find($request['cliente_id']);

        if ( (!is_null($empresa)) && (!is_null($cliente)) ) { 

            $ult_venta = $empresa->facturas_ventas->where('es_ajuste','=', 0)->last();
            if ( is_null( $ult_venta ) ) {
                $num_doc = "FV001";
            } else {
                $num_doc = substr( $ult_venta->num_doc, 4, strlen( $ult_venta->num_doc )) + 1;
                $num_doc = "FV00".$num_doc;

            }

            $venta = new Ventas([
                'empresa_id'=>$empresa->id,
                'creador_id'=>\Auth::user()->entidad_id,
                'cliente_id'=>$cliente->id,
                'num_doc'=>$num_doc,
                'fecha'=>$request['fecha'],
                'hora'=>$request['hora']
            ]);  
            $venta->save(); 
            $mensaje = "Registro creado con éxito, incluye los articulos a vender";            
            \Session::flash('msgExito', $mensaje );
            return redirect('/Venta/Ver/'.$venta->id);
        }else {
            $mensaje = "Error al ubicar la ficha de la empresa o el cliente seleccionado";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    public function update(Request $request)
    {
        $cliente = Entidades::find($request['cliente_id']);
        $venta = Ventas::find($request['id']);
        if ( (!is_null($cliente)) && (!is_null($venta) ) ) {  
            $venta->cliente_id = $cliente->id;
            $venta->fecha = $request['fecha'];
            $venta->hora = $request['hora'];
            $venta->save(); 
            $mensaje = "Registro actualizado con éxito, incluye los articulos a vender";            
            \Session::flash('msgExito', $mensaje );
            return back();
        }else {
            $mensaje = "Error al ubicar la ficha del cliente o la venta";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }
    public function store_lin(Request $request)
    {
        $venta = Ventas::find($request['venta_id']);
        $articulo = Productos::find($request['articulo_id']);
        if ( (!is_null($venta)) && (!is_null($articulo)) ) { 
            $can = $request['cantidad'];
            if ( $can < 1 ) {
                $can = 1;
            }
            $pre = $articulo->precio;
            $imp = $can * $pre;
            $linea_venta = Movimientos::where([
                                    ['articulo_id','=',$articulo->id],
                                    ['factura_venta_id', '=', $venta->id]
                                ])->first();
            if ( is_null($linea_venta)) {
            	if ( $articulo->existencia < $can ) {
		            $mensaje = "Error la cantidad indicada supera la existencia del Artículo";          
		            \Session::flash('msgError', $mensaje );
		            return back();
            	}else {
	                $linea_venta = new Movimientos([
	                    'articulo_id'=>$articulo->id,
	                    'factura_venta_id'=>$venta->id,
	                    'fecha'=>$venta->fecha,
	                    'hora'=>$venta->hora,
	                    'cantidad'=>$can,
	                    'precio'=>$pre,
	                    'importe'=>$imp
	                ]);  
	                $linea_venta->save();
            	}            	
            }else{
                $cantidad_ant = $linea_venta->cantidad;
                $can = $linea_venta->cantidad + $can;
                $imp = $can * $pre;
                $linea_venta->cantidad = $can;
                $linea_venta->importe = $imp;
                $linea_venta->save();
            	
                $articulo = Productos::find($articulo->id); 
                if ( $articulo->existencia < 0 ) {
		            $mensaje = "Error la cantidad indicada supera la existencia del Artículo"; 
                    $linea_venta->cantidad = $cantidad_ant;    
                    $linea_venta->save();          
		            \Session::flash('msgError', $mensaje );
		            return back();
            	}           	
            }
            $mensaje = "Registro creado con éxito";            
            \Session::flash('msgExito', $mensaje );            
            return back();
        }else {
            $mensaje = "Error al ubicar la venta o el artículo seleccionado";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    public function update_lin(Request $request)
    {
        $venta = Ventas::find($request['venta_id']);
        $linea_venta = Movimientos::where([
                                    ['factura_venta_id', '=', $venta->id],
                                    ['id', '=', $request['linea_id']]
                                ])->first();
         $articulo = Productos::find($request['articulo_id']);
        if ( (!is_null($linea_venta)) && (!is_null($venta) ) && (!is_null($articulo) ) ) {  
            
            $can = $request['cantidad'];
            if ( $can < 1 ) {
                $can = 1;
            }

            $cantidad_ant = $linea_venta->cantidad;
            $pre = $articulo->precio;
            $imp = $can * $pre;
            $linea_venta->articulo_id = $articulo->id;
            $linea_venta->cantidad = $can;
            $linea_venta->precio = $pre;
            $linea_venta->importe = $imp;
            $linea_venta->save();

            $articulo = Productos::find($articulo->id);            
            if ( $articulo->existencia < 0 ) {
	            $mensaje = "Error la cantidad indicada supera la existencia del Artículo";   
                $linea_venta->cantidad = $cantidad_ant;    
                $linea_venta->save();   
	            \Session::flash('msgError', $mensaje );
	            return back();
            }

            $mensaje = "Registro actualizado con éxito";            
            \Session::flash('msgExito', $mensaje );
            return back();

        }else {
            $mensaje = "Error al ubicar la ficha de la empresa o el venta";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    public function destroy_lin(Request $request)
    {
        $venta = Ventas::find($request['id']);
        $linea_venta = Movimientos::where([
                                    ['factura_venta_id', '=', $venta->id],
                                    ['id', '=', $request['linea_id']]
                                ])->first();
        if ( (!is_null($linea_venta)) && (!is_null($venta) ) ) {  
            $linea_venta->delete();
        }
    }

    public function load_lin(Request $request)
    {
        $venta = Ventas::find($request['id']);
        $tabla  = "<table class='table table-bordered' id='movimientos'>
					<thead>
					  <tr>
						<th style='text-align:center; width: 10px'><input type='checkbox' id='checkAllLineas' onchange='selectAllLineas(this);'></th>
						<th style='width: 10px'>#</th>
						<th style='text-align:center; width: 160px;'>Articulo</th>
						<th style='text-align:center; width: 100px;'>Existencia</th>
						<th style='text-align:center; width: 100px;'>Cantidad</th>
						<th style='text-align:center; width: 120px;'>Precio</th>
						<th style='text-align:center; width: 120px;'>Importe</th>
					  </tr> 
					</thead> 
					<tbody>";
        if ( !is_null($venta) )  {
          $movimientos = $venta->movimientos;
          if ( sizeof( $movimientos ) > 0 ){
            $cont = 0;
            foreach ($movimientos as $item):
              $cont = $cont + 1;
              if($item->articulo->existencia < $item->cantidad) { $style="text-align:center; color: red;"; }else{ $style="text-align:center;"; }
              $tabla .="<tr>
                          <th style='text-align:center;'>
                            <input type='checkbox' value='".$item->id."*".$item->articulo_id."*".$item->cantidad."*".$item->precio."*".$item->importe."'>
                          </th>
                          <td>".$cont."</td>
                          <td>".$item->articulo->nombre."</td>
                          <td style='".$style."'>".$item->articulo->existencia."</td>
                          <td>".$item->cantidad."</td>
                          <td>".$item->precio."</td>
                          <td>".$item->importe."</td>
                        </tr>";                                               
            endforeach;
          }
        }        
        $tabla .= " </tbody>
                  </table>";
        return $tabla;
    }

    public function destroy($id)
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        $venta = Ventas::where([
                                    ['id', '=', $id],
                                    ['empresa_id', '=', $empresa->id]
                                ])->first();
        if ( !is_null($venta) ) {            
            Movimientos::where('factura_venta_id', '=', $venta->id)->delete();
            $venta->delete();
            $mensaje = "Registro eliminado con éxito";            
            \Session::flash('msgExito', $mensaje );
            return redirect('ver_ventas');
        }else {
            $mensaje = "Error al ubicar la ficha del documento";          
            \Session::flash('msgError', $mensaje );            
            return redirect('ver_ventas');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $venta = Ventas::find($id);
        if ( !is_null($venta) ) {
          return view('Ventas.formVenta', compact('venta'));        
        }else {
          $mensaje = "Error al ubicar el venta seleccionado";          
          \Session::flash('msgError', $mensaje );
          return back();
        }
    }
}
