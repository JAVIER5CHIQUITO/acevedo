@extends('layouts.header')

@section('content')

@if(Auth::user())

  @include('layouts.menu')
  @include('layouts.panel-admin')

@endif

 <div class="col-md-9">
  <div class="panel panel-default">
  <div class="panel-heading" style="background-color:  #095f59;">
    <h3 class="panel-title">Website Overview</h3>
  </div>
  <div class="panel-body">
   <div class="col-md-3">
     <div class="well dash-box">
       <h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span> 12</h2>
       <h4>Users</h4>
     </div>
   </div>
   <div class="col-md-3">
     <div class="well dash-box">
       <h2><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> 25</h2>
       <h4>Pages</h4>
     </div>
   </div>
   <div class="col-md-3">
     <div class="well dash-box">
       <h2><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>126</h2>
       <h4>Posts</h4>
     </div>
   </div>
   <div class="col-md-3">
     <div class="well dash-box">
       <h2><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> 2129</h2>
       <h4>Visitores</h4>
     </div>
   </div>
  </div>
</div>
<!--Latest User-->
<div class="panel panel-default">
  <div class="panel-heading" style="background-color:#095f59">
    <h3 class="panel-title">Latest Users</h3>
  </div>
  <div class="panel-body">
    <table data-toggle="table" data-toolbar="#toolbar" data-locale="es-ES" data-search="true" data-show-refresh="true">
    <thead>
    <tr>
        <th>Name</th>
        <th>Stars</th>
        <th>Forks</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
          Nombre
        </td>
        <td>Apellido</td>
        <td>Cedula</td>
        <td>Empresa 
        </td>
    </tr>
    
    <tr>
      <td>
          Nombre
        </td>
        <td>Apellido</td>
        <td>Cedula</td>
        <td>Empresa 
        </td>
    </tr>
    
    <tr>
      <td>
          Nombre
        </td>
        <td>Apellido</td>
        <td>Cedula</td>
        <td>Empresa 
        </td>
    </tr>
    
    </tbody>
</table>

  </div>
</div>

      </div>
    </div>
  </div>
</section>
@endsection