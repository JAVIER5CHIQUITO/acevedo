<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras extends Model
{
    //
    protected $table='facturas_compras';
    
    protected $fillable = ['empresa_id','creador_id','proveedor_id','fecha','hora','num_doc','num_fac_prv','es_ajuste','total'];

	public function movimientos(){
    	return $this->hasMany( Movimientos::class, 'factura_compra_id', 'id');
    }

    public function empresa(){
    	return $this->belongsTo( Empresa::class );
    }

    public function proveedor(){
    	return $this->belongsTo( Entidades::class );
    }
}
