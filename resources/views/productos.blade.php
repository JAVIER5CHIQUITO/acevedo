       <section class="banner-04">
        <div class="container">
            <div id="carousel-example-04" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">

                        <div class="row">
                            <div class="col-md-4 content-sec">
                                <h1>This place is reserved for heading tag</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                            </div>
                            <div class="col-md-6  col-md-offset-2">
                                <img class="carusel-img" src="{{asset('imagenes/producto-1.jpg')}}">
                            </div>
                        </div>

                    </div>

                    <div class="item">

                        <div class="row">
                            <div class="col-md-4 content-sec">
                                <h1>This place is reserved for heading tag</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                            </div>
                            <div class="col-md-6  col-md-offset-2">
                                <img class="carusel-img" src="{{asset('imagenes/producto-2.jpg')}}">
                            </div>
                        </div>

                    </div>

                </div>
                <div class="small-width-navigation">
                    <a class="left carousel-control" href="#carousel-example-04" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-04" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <!-- Controls -->
            </div>
        </div>
    </section>