<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratos extends Model
{
    //
    protected $table='contratos';
    
    protected $fillable = ['entidad_id','empresa_id','sueldo','fecha'];
}
