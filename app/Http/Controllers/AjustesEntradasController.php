<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Compras;
use App\Productos;
use App\Movimientos;
use App\Entidades;


class AjustesEntradasController extends Controller
{
    //
    public function index()
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		if ( !is_null($empresa) ) {
			$facturas_compras = $empresa->facturas_compras->where('es_ajuste','=', 1);
			return view('compras.ajustes', compact('facturas_compras'));
		}else {
			return view('compras.ajustes');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$compra = Compras::find($id);
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$articulos = $empresa->articulos;
		return view('compras.formAjuste', compact('articulos', 'compra'));
	}

	public function show_form()
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$articulos = $empresa->articulos;
		return view('compras.formAjuste', compact('articulos'));
	}

	public function store(Request $request)
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$proveedor = Entidades::find(\Auth::user()->entidad_id);

		if ( (!is_null($empresa)) && (!is_null($proveedor)) ) {  

			$ult_ajuste = $empresa->facturas_compras->where('es_ajuste','=', 1)->last();
			if ( is_null( $ult_ajuste ) ) {
				$num_doc = "AJE001";
			} else {
				$num_doc = substr( $ult_ajuste->num_doc, 5, strlen( $ult_ajuste->num_doc )) + 1;
				$num_doc = "AJE00".$num_doc;

			}

			$compra = new Compras([
				'empresa_id'=>$empresa->id,
				'creador_id'=>\Auth::user()->entidad_id,
				'proveedor_id'=>$proveedor->id,
				'es_ajuste'=> 1,
				'num_doc'=> $num_doc,
				'num_fac_prv'=> $num_doc,
				'fecha'=>$request['fecha'],
				'hora'=>$request['hora']
			]);  
			$compra->save(); 
			$mensaje = "Registro creado con éxito, incluye los articulos a ajustar";            
			\Session::flash('msgExito', $mensaje );
			return redirect('/Ajuste/Entrada/Ver/'.$compra->id);
		}else {
			$mensaje = "Error al ubicar la ficha de la empresa o el proveedor seleccionado";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}

	public function update(Request $request)
	{
		$proveedor = Entidades::find(\Auth::user()->entidad_id);
		$compra = Compras::find($request['id']);
		if ( (!is_null($proveedor)) && (!is_null($compra) ) ) {  
			$compra->proveedor_id = $proveedor->id;
			$compra->fecha = $request['fecha'];
			$compra->hora = $request['hora'];
			$compra->save(); 
			$mensaje = "Registro actualizado con éxito, incluye los articulos a ajustar";            
			\Session::flash('msgExito', $mensaje );
			return back();
		}else {
			$mensaje = "Error al ubicar la ficha de la empresa o el ajuste";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}

	public function destroy($id)
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$compra = Compras::where([
									['id', '=', $id],
									['empresa_id', '=', $empresa->id]
								])->first();
		if ( !is_null($compra) ) {
			Movimientos::where('factura_compra_id', '=', $compra->id)->delete();
			$compra->delete();
			$mensaje = "Registro eliminado con éxito";            
			\Session::flash('msgExito', $mensaje );
			return redirect('ver_ajustes_entradas');
		}else {
			$mensaje = "Error al ubicar la ficha del Ajsute";          
			\Session::flash('msgError', $mensaje );            
			return redirect('ver_ajustes_entradas');
		}
	}
    
}
