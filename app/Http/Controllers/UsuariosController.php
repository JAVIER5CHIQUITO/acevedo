<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entidades;
use App\User;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('usuarios.aprobar_usuarios');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lista()
    {
       $usuarios = User::all();
       $data = array();
        
        foreach($usuarios as $value){
         
         $entidad = Entidades::find($value->entidad_id);
          
          if($entidad->es_proveedor != 0){
            $tipo_e = 'Proveedor';
          }elseif($entidad->es_cliente != 0){
            $tipo_e = 'Cliente';
          }elseif($entidad->es_empleado != 0){
            $tipo_e = 'Empleado';
          }
          
          $btn = '<a class="btn btn-warning aprob" id="aprob" title="Aprobar" data-id="'.$value->id.'" >Aprobar</a> <a class="btn btn-danger rechazar" id="rechazar" data-id="'.$value->id.' title="Rechazar" >Rechazar</a>';
          
          $vector= array('nombre'=>$value->user,'correo'=>$value->email,'id_entidad'=>$entidad->id,'entidad'=>$tipo_e,'opciones'=>$btn);
         
          array_push($data,$vector);
        }
         return response()->json($data);
    }
    
    public function aprobar(Request $request)
    {
       //dd();
       $user = User::find($request->id);
       $user->aprobado = 1;
       $user->save();
       return response()->json('success');
    }
    
    public function rechazar(Request $request)
    {
       $user = User::find($request->id);
       $user->aprobado = 3;
       $user->save();
      
       return response()->json('success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    
    public function proveedor()
    {
      return view('usuarios.proveedores');
    }
    
    public function dataproveedor()
    {
       $tipo_e = 'Proveedor';
       $entidades = Entidades::where('es_proveedor',1)->get();
       $data = array();
        
        foreach($entidades as $value){
         //$usuario = User::find($value->entidad_id);
        $vector= array('nombre'=>$value->nombre." ".$value->apellido,'cedula'=>$value->cedula,'correo'=>$value->email,'id_entidad'=>$value->id,'entidad'=>$tipo_e);
         
          array_push($data,$vector);
        }
         return response()->json($data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function cliente()
    {
      return view('usuarios.clientes');
    }
    
    public function datacliente()
    {
       $tipo_e = 'Cliente';
       $entidades = Entidades::where('es_cliente',1)->get();
       $data = array();
        
        foreach($entidades as $value){
         //$usuario = User::find($value->entidad_id);
        $vector= array('nombre'=>$value->nombre." ".$value->apellido,'cedula'=>$value->cedula,'correo'=>$value->email,'id_entidad'=>$value->id,'entidad'=>$tipo_e);
         
          array_push($data,$vector);
        }
         return response()->json($data);
    }
    
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function empleado()
    {
      return view('usuarios.empleados');
    }
    
    public function dataempleado()
    {
       $tipo_e = 'Empleado';
       $entidades = Entidades::where('es_empleado',1)->get();
       $data = array();
        
        foreach($entidades as $value){
         //$usuario = User::find($value->entidad_id);
        $vector= array('nombre'=>$value->nombre." ".$value->apellido,'cedula'=>$value->cedula,'correo'=>$value->email,'id_entidad'=>$value->id,'entidad'=>$tipo_e);
         
          array_push($data,$vector);
        }
         return response()->json($data);
    }
    
    public function destroy($id)
    {
        //
    }
}
