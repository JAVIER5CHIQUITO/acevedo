@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

<div class="col-md-9">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default overflow-hidden">
      <div class="panel-heading panel-color" style="background-color:#095f59; color: #ffffff;">
        <h3 class="panel-title"> Listado </h3>
      </div>
      <div class="panel-body">
        <!-- data-toggle="table" data-toolbar="#toolbar" data-locale="es-ES" data-search="true" data-show-refresh="true" -->
        <a href="{{url('/Compra/Nueva')}}" title="Nueva" class="btn btn-primary btn-xs reportHide">
          <i class="glyphicon glyphicon-plus"></i> Nueva
        </a>
        <table data-toggle="table" data-toolbar="#toolbar" data-locale="es-ES" data-search="true" data-show-refresh="true">
          <thead>
            <tr>
                <th style='text-align:center; width: 40px;'>#</th>
                <th style='text-align:center; width: 70px;'>Fecha</th>
                <th style='text-align:center; width: 70px;'>Hora</th>
                <th style='text-align:center; width: 70px;'>Nº Documento</th>
                <th style='text-align:center; width: 70px;'>Nº Factura Proveedor</th>
                <th style='text-align:center; width: 300px;'>Proveedor</th>
                <th style='text-align:center; width: 300px;'>Total</th>
                <th style='text-align:center;'>Acciones</th>
            </tr>
          </thead>
          <tbody>
            @if( isset( $facturas_compras ) )
              @if( sizeof( $facturas_compras) > 0 )
                <?php $cont = 0; ?>
                @foreach ($facturas_compras as $item)
                  <?php $cont = $cont + 1; ?>
                  <tr>
                    <td style='text-align:right;'>{{$cont}}</td>
                    <td style='text-align:right;'>{{$item->fecha}}</td>
                    <td style='text-align:right;'>{{$item->hora}}</td> 
                    <td style='text-align:right;'>{{$item->num_doc}}</td>  
                    <td style='text-align:right;'>{{$item->num_fac_prv}}</td>  
                    <td style='text-align:left;'>{{$item->proveedor->nombre." ".$item->proveedor->apellido}}</td>  
                    <td style='text-align:left;'>{{$item->total}}</td>  
                    <td style='text-align:center;' class="reportHide">
                      <a href="{{url('/Compra/Ver/'.$item->id)}}" title="Ver" class="btn btn-info btn-xs reportHide">
                        <i class="glyphicon glyphicon-eye-open"></i>
                      </a>
                      <a href="{{url('/Compra/Eliminar/'.$item->id)}}" title="Eliminar" class="btn btn-danger btn-xs reportHide" >
                        <i class="glyphicon glyphicon-trash"></i>
                      </a>                      
                    </td>
                  </tr>
                @endforeach
              @else 
                <tr>
                  <td colspan="8">
                    <div class="alert alert-info alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-info"></i> Aviso!</h4>
                      No hay ninguna factura de Compra registrada.
                    </div>
                  </td>
                </tr>          
              @endif
            @else
              <tr>
                <td colspan="8">
                  <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso!</h4>
                    No hay ninguna factura de Compra registrada.
                  </div>
                </td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection