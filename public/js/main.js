  function htmlbodyHeightUpdate() {
    var height3 = $(window).height()
    var height1 = $('.nav').height() + 50
    height2 = $('.main').height()
    if (height2 > height3) {
      $('html').height(Math.max(height1, height3, height2) + 10);
      $('body').height(Math.max(height1, height3, height2) + 10);
    }
    else {
      $('html').height(Math.max(height1, height3, height2));
      $('body').height(Math.max(height1, height3, height2));
    }

  }

  $(document).ready(function () {
    htmlbodyHeightUpdate()
    $(window).resize(function () {
      htmlbodyHeightUpdate()
    });
    $(window).scroll(function () {
      height2 = $('.main').height()
      htmlbodyHeightUpdate()
    });

    $('tbody').on('click', '.detalles', function (e) {
      e.preventDefault();
      var url = $(this).data('url');
      var cont = $('.cont');
      cont.html('');
      $.ajax({
        url: url,
        type: 'GET',
        dataType: 'JSON',
      }).done(function (data) {

        $('#nombre').text(data.nombre);
        $('#telefono').text(data.telefono);
        $('#correo').text(data.correo);
        $('#direccion').text(data.direccion);
        $('#usuario_mrky').text(data.user_mariky);

      });

    });
    //ver

    $('tbody').on('click', '#aprob', function (event) {
      event.preventDefault();
      //	alert('aqui');
      var a = $(this);
      var id = $(this).data('id');
      var td = $(this).parents('td');
      $.ajax({
        url: 'aprobar_u',
        type: 'POST',
        dataType: 'JSON',
        data: { id: id },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function () {
          // alerta.html('Cargando...');
          // ordenes.html('Cargando...');
          a.html('...');
        },
        error: function (result) {
          //alerta.html('error..');
          console.log(result);
        },
        success: function (result) {
          //alert(id);
          alert('Usuario Aprobado');
          td.html('<h4 class="text text-info">Aprobado</h4>');
          $('button[name="refresh"]').click(); //para refrescar la tabla luego de hacer la accion
        },
      });

    });

  });
  