<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Pedidos;
use App\Productos;
use App\Lineas_Pedido;
use App\Ventas;
use App\Movimientos;

class PedidosController extends Controller
{
    //
    public function index()
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        if ( !is_null($empresa) ) {
            $pedidos = $empresa->pedidos;
            return view('pedidos.pedidos', compact('pedidos'));
        }else {
            return view('pedidos.pedidos');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function facturar(Request $request)
    {
        $empresa = Empresa::find($request['empresa_id']);
        if ( !is_null($empresa) ) {   
          
          $pedido = Pedidos::find($request['id']);
          if ( !is_null($pedido) ) {

            $vBoolExtNegativa = FALSE;
            $lineas = $pedido->lineas;

            if ( sizeof( $lineas ) > 0 ) {

              foreach ($lineas as $item):
                if( $item->articulo->existencia < $item->cantidad){
                  $vBoolExtNegativa = TRUE;
                }
              endforeach;

              if ( $vBoolExtNegativa == TRUE ) {
                $mensaje = "Uno de los items posee una existencia menor a la cantidad pedida";
                \Session::flash('msgError', $mensaje );
                return back();
              }

              $pedido->status = 1;
              $pedido->save();
              $venta = new Ventas([
                  'empresa_id'=>$pedido->empresa_id,
                  'creador_id'=>\Auth::user()->entidad_id,
                  'cliente_id'=>$pedido->cliente_id,
                  'num_doc'=>$pedido->num_doc,
                  'fecha'=>$pedido->fecha,
                  'hora'=>$pedido->hora
              ]); 
              $venta->save(); 

              foreach ($lineas as $item):                
                $movimiento = new Movimientos([
                  'articulo_id'=>$item->articulo_id,
                  'factura_venta_id'=>$venta->id,
                  'fecha'=>$venta->fecha,
                  'hora'=>$venta->hora,
                  'cantidad'=>$item->cantidad,
                  'precio'=>$item->precio,
                  'importe'=>$item->importe
                ]); 
                $movimiento->save();
              endforeach;

              $mensaje = "Factura creada con éxito";            
              \Session::flash('msgExito', $mensaje );
              return back();

            }else{
              $mensaje = "Error, no se puede generar una factura sin lineas";          
              \Session::flash('msgError', $mensaje );
              return back();
            }

          }else {
            $mensaje = "Error al ubicar la ficha del pedido";          
            \Session::flash('msgError', $mensaje );
            return back();
          }

        }else {
          $mensaje = "Error al ubicar la ficha de la empresa";          
          \Session::flash('msgError', $mensaje );
          return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empresa = Empresa::find($request['empresa_id']);
        if ( !is_null($empresa) ) {            
            $pedido = new Pedidos([
                'empresa_id'=>$empresa->id,
                'cliente_id'=>\Auth::user()->entidad_id,
                'fecha'=>$request['fecha'],
                'hora'=>$request['hora']
            ]);  
            $pedido->save(); 
            $num_doc = "PDV00".$pedido->id;
            $pedido->num_doc = $num_doc;
            $pedido->save(); 
            $mensaje = "Registro creado con éxito, incluye los articulos a pedir";            
            \Session::flash('msgExito', $mensaje );
            return redirect('/Pedido/Ver/'.$pedido->id);
        }else {
            $mensaje = "Error al ubicar la ficha de la empresa";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }
    public function store_lin(Request $request)
    {
        $pedido = Pedidos::find($request['pedido_id']);
        $articulo = Productos::find($request['articulo_id']);
        if ( (!is_null($pedido)) && (!is_null($articulo)) ) { 
            $can = $request['cantidad'];
            if ( $can < 1 ) {
                $can = 1;
            }
            $pre = $articulo->precio;
            $imp = $can * $pre;
            $linea_pedido = Lineas_Pedido::where([
                                    ['articulo_id','=',$articulo->id],
                                    ['pedido_venta_id', '=', $pedido->id]
                                ])->first();
            if ( is_null($linea_pedido)) {
                $linea_pedido = new Lineas_Pedido([
                    'articulo_id'=>$articulo->id,
                    'pedido_venta_id'=>$pedido->id,
                    'cantidad'=>$can,
                    'precio'=>$pre,
                    'importe'=>$imp
                ]);  
                $linea_pedido->save();
                $total_pedido = $pedido->total;
                $pedido->total = $total_pedido + $imp;
                $pedido->save();
            }else{
                $imp_ant = $linea_pedido->importe;
                $can = $linea_pedido->cantidad + $can;
                $imp = $can * $pre;
                $linea_pedido->cantidad = $can;
                $linea_pedido->importe = $imp;
                $linea_pedido->save();

                $total_pedido = $pedido->total - $imp_ant;
                $pedido->total = $total_pedido + $imp;
                $pedido->save();
            }
            $mensaje = "Registro creado con éxito";            
            \Session::flash('msgExito', $mensaje );            
            return back();
        }else {
            $mensaje = "Error al ubicar el pedido o el artículo seleccionado";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = Pedidos::find($id);
        $empresas = Empresa::all();
        return view('pedidos', compact('empresas', 'pedido'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido = Pedidos::find($id);
        if ( !is_null($pedido) ) {
          return view('pedidos.formPedido', compact('pedido'));        
        }else {
          $mensaje = "Error al ubicar el pedido seleccionado";          
          \Session::flash('msgError', $mensaje );
          return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $empresa = Empresa::find($request['empresa_id']);
        $pedido = Pedidos::find($request['pedido_id']);
        if ( (!is_null($empresa)) && (!is_null($pedido) ) ) {  
            $pedido->empresa_id = $empresa->id;
            $pedido->fecha = $request['fecha'];
            $pedido->hora = $request['hora'];
            $pedido->save(); 
            $mensaje = "Registro actualizado con éxito, incluye los articulos a pedir";            
            \Session::flash('msgExito', $mensaje );
            return back();
        }else {
            $mensaje = "Error al ubicar la ficha de la empresa o el pedido";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    public function update_lin(Request $request)
    {
        $pedido = Pedidos::find($request['pedido_id']);
        $linea_pedido = Lineas_Pedido::where([
                                    ['pedido_venta_id', '=', $pedido->id],
                                    ['id', '=', $request['linea_id']]
                                ])->first();
         $articulo = Productos::find($request['articulo_id']);
        if ( (!is_null($linea_pedido)) && (!is_null($pedido) ) && (!is_null($articulo) ) ) {  
            $can = $request['cantidad'];
            if ( $can < 1 ) {
                $can = 1;
            }
            $imp_ant = $linea_pedido->importe;
            $pre = $articulo->precio;
            $imp = $can * $pre;
            $linea_pedido->articulo_id = $articulo->id;
            $linea_pedido->cantidad = $can;
            $linea_pedido->precio = $pre;
            $linea_pedido->importe = $imp;
            $linea_pedido->save();

            $total_pedido = $pedido->total - $imp_ant;
            $pedido->total = $total_pedido + $imp;
            $pedido->save();
            $mensaje = "Registro actualizado con éxito";            
            \Session::flash('msgExito', $mensaje );
            return back();
        }else {
            $mensaje = "Error al ubicar la ficha de la empresa o el pedido";          
            \Session::flash('msgError', $mensaje );
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        $pedido = Pedidos::where([
                                    ['id', '=', $id],
                                    ['empresa_id', '=', $empresa->id]
                                ])->first();
        if ( !is_null($pedido) ) {
            $pedido->delete();
            $mensaje = "Registro eliminado con éxito";            
            \Session::flash('msgExito', $mensaje );
            return redirect('ver_pedidos');
        }else {
            $mensaje = "Error al ubicar la ficha del Pedido";          
            \Session::flash('msgError', $mensaje );            
            return redirect('ver_pedidos');
        }
    }

    public function destroy_lin(Request $request)
    {
        $pedido = Pedidos::find($request['pedido_id']);
        $linea_pedido = Lineas_Pedido::where([
                                    ['pedido_venta_id', '=', $pedido->id],
                                    ['id', '=', $request['linea_id']]
                                ])->first();
        if ( (!is_null($linea_pedido)) && (!is_null($pedido) ) ) { 
            $total_pedido = $pedido->total;
            $pedido->total = $total_pedido - $linea_pedido->importe;
            $pedido->save(); 
            $linea_pedido->delete();
        }
    }

    public function load_lin(Request $request)
    {
        $pedido = Pedidos::find($request['pedido_id']);
        $tabla  = "<table id='lineas_pedido' class='table table-striped table-bordered table-hover'>
                    <thead>
                      <tr>
                        <th style='text-align:center; width: 10px'><input type='checkbox' id='checkAllLineas' onchange='selectAllLineas(this);'></th>
                        <th style='text-align:center; width: 15px;'>#</th>
                        <th style='text-align:center;'>Articulo</th>
                        <th style='text-align:center; width: 150px;'>Cantidad</th>
                        <th style='text-align:center; width: 150px;'>Precio</th>
                        <th style='text-align:center; width: 150px;'>Importe</th>
                      </tr>
                    </thead>
                    <tbody>";
        if ( !is_null($pedido) )  {
          $lineas = $pedido->lineas;
          if ( sizeof( $lineas ) > 0 ){
            $cont = 0;
            foreach ($lineas as $item):
              $cont = $cont + 1;
              $tabla .="<tr>
                          <th style='text-align:center;'>
                            <input type='checkbox' value='".$item->id."*".$item->articulo_id."*".$item->cantidad."*".$item->precio."*".$item->importe."'>
                          </th>
                          <td>".$cont."</td>
                          <td>".$item->articulo->nombre."</td>
                          <td>".$item->cantidad."</td>
                          <td>".$item->precio."</td>
                          <td>".$item->importe."</td>
                        </tr>";                                               
            endforeach;
          }
        }        
        $tabla .= " </tbody>
                  </table>";
        return $tabla;
    }
}
