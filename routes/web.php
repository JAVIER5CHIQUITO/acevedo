<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
//rutas por fuera del admin
Route::get('/','HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/historia', 'HomeController@history');
Route::get('/ubicacion', 'HomeController@ubicacion');
Route::get('/pedidos', 'HomeController@pedidos');

//rutas admin
Route::get('/dashboard', 'HomeController@dashboard')->middleware('auth');
//usuarios
Route::get('aprobar_usuarios','UsuariosController@index');
Route::get('listar_usuarios','UsuariosController@lista'); //json
Route::post('aprobar_u','UsuariosController@aprobar');
Route::post('rechazar_u','UsuariosController@rechazar');

Route::get('ver_proveedores','UsuariosController@proveedor');
Route::get('proveedores','UsuariosController@dataproveedor'); //json

Route::get('ver_clientes','UsuariosController@cliente');
Route::get('clientes','UsuariosController@datacliente'); //json

Route::get('ver_empleados','UsuariosController@empleado');
Route::get('empleados','UsuariosController@dataempleado'); //json

// ---------------------------------------------------------------------------------------------------------------------------
// Productos
Route::get('ver_productos','ProductosController@index');
Route::post('Guardar_Articulo', 'ProductosController@store');
Route::get('/Articulos/Eliminar/{id}', 'ProductosController@destroy');
Route::get('/Articulos/Ver/{id}', 'ProductosController@show');
Route::post('Editar_Articulo', 'ProductosController@update');
Route::post('cargar_articulos_emp', 'ProductosController@load');
Route::post('cargar_precio_art', 'ProductosController@load_precio');

// Empresa
Route::get('ver_empresa', 'EmpresasController@index');
Route::post('Guardar_Empresa', 'EmpresasController@store');

// Pedidos
Route::post('Nuevo_Pedido', 'PedidosController@store');
Route::get('/Pedido/Ver/{id}', 'PedidosController@show');
Route::post('Editar_Pedido', 'PedidosController@update');

Route::get('ver_pedidos','PedidosController@index');
Route::get('/Pedido/Venta/Ver/{id}', 'PedidosController@edit');
Route::post('Confirmar_Pedido', 'PedidosController@facturar');
Route::get('/Pedido/Eliminar/{id}', 'PedidosController@destroy');

// Lineas Pedidos
Route::post('cargar_lineas_pedido', 'PedidosController@load_lin');
Route::post('Nueva_Linea_Pedido', 'PedidosController@store_lin');
Route::post('Editar_Linea_Pedido', 'PedidosController@update_lin');
Route::post('Eliminar_linea_pedido', 'PedidosController@destroy_lin');

// Ventas
Route::get('ver_ventas','VentasController@index');
Route::get('/Venta/Nueva', 'VentasController@show_form');
Route::get('/Venta/Ver/{id}', 'VentasController@show');
Route::get('/Venta/Eliminar/{id}', 'VentasController@destroy');
Route::post('Nueva_Venta', 'VentasController@store');
Route::post('Editar_Venta', 'VentasController@update');
Route::post('Nueva_Linea_Venta', 'VentasController@store_lin');
Route::post('Editar_Linea_Venta', 'VentasController@update_lin');
Route::post('Eliminar_linea_venta', 'VentasController@destroy_lin');
Route::post('cargar_movimientos', 'VentasController@load_lin');

// Compras
Route::get('ver_compras','ComprasController@index');
Route::get('/Compra/Nueva', 'ComprasController@show_form');
Route::get('/Compra/Ver/{id}', 'ComprasController@show');
Route::get('/Compra/Eliminar/{id}', 'ComprasController@destroy');
Route::post('Nueva_Compra', 'ComprasController@store');
Route::post('Editar_Compra', 'ComprasController@update');
Route::post('Nueva_Linea_Compra', 'ComprasController@store_lin');
Route::post('Editar_Linea_Compra', 'ComprasController@update_lin');
Route::post('Eliminar_linea_compra', 'ComprasController@destroy_lin');
Route::post('cargar_movimientos_compra', 'ComprasController@load_lin');

// Ajustes de inventario - Entradas -
Route::get('ver_ajustes_entradas','AjustesEntradasController@index');
Route::get('/Ajuste/Entrada/Nuevo', 'AjustesEntradasController@show_form');
Route::get('/Ajuste/Entrada/Ver/{id}', 'AjustesEntradasController@show');
Route::get('/Ajuste/Entrada/Eliminar/{id}', 'AjustesEntradasController@destroy');
Route::post('Nuevo_Ajuste_Entrada', 'AjustesEntradasController@store');
Route::post('Editar_Ajuste_Entrada', 'AjustesEntradasController@update');

// Ajustes de inventario - Salidas -
Route::get('ver_ajustes_salidas','AjustesSalidasController@index');
Route::get('/Ajuste/Salida/Nuevo', 'AjustesSalidasController@show_form');
Route::get('/Ajuste/Salida/Ver/{id}', 'AjustesSalidasController@show');
Route::get('/Ajuste/Salida/Eliminar/{id}', 'AjustesSalidasController@destroy');
Route::post('Nuevo_Ajuste_Salida', 'AjustesSalidasController@store');
Route::post('Editar_Ajuste_Salida', 'AjustesSalidasController@update');
// ---------------------------------------------------------------------------------------------------------------------------