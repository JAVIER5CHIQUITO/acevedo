<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function history()
    {
        return view('empresa');
    }
    
    public function ubicacion()
    {
        return view('ubicacion');
    }
    
    public function dashboard()
    {
        return view('admin.panel-admin');
    }
    
    public function pedidos()
    {
        $empresas = Empresa::all();
        return view('pedidos', compact('empresas'));
    }
    
}
