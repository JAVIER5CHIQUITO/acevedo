@extends('layouts.header')

@section('content')

@include('layouts.panel-inicio')
	<div class="container">
		<div class="form-reg">
			<h3 class="panel-heading form-titulo"> @if ( isset($pedido) ) Editar @else Nuevo @endif Pedido</h3>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br>						
						<form id="demo-form2" data-parsley-validate="" @if ( isset($pedido) ) action="{{url('Editar_Pedido')}}" @else action="{{url('Nuevo_Pedido')}}" @endif method="POST" class="form-horizontal form-label-left" novalidate="">					
							{{ csrf_field() }}
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_id"></label>
								<div class="col-md-6 col-sm-6 col-xs-12">                  
									<select class="form-control" id="empresa_id" name="empresa_id" required="required" onchange="cargar_articulos(this);" title="Empresa">										
										<option value="" @if ( !isset($pedido) ) selected="selected" @endif >Seleeccione la Empresa..</option>
										@foreach ($empresas as $item)										
											@if ( isset($pedido) && ( $pedido->empresa_id == $item->id) )
												<option value="{{$item->id}}" selected="selected">{{$item->nombre}}</option>
											@else
												<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endif
										@endforeach
								 	</select>
								</div>
								@if ( isset($pedido) )
									<input type='hidden' name="pedido_id" id="pedido_id" value="{{$pedido->id}}">
								@endif
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha"></label>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<input type="date" id="fecha" name="fecha" @if ( isset($pedido) ) value="{{$pedido->fecha}}" @else value="<?php $fecha= date("Y-m-d"); echo $fecha; ?>" @endif required="required" class="form-control col-md-7 col-xs-12" title="Fecha">
								</div>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<input type="time" id="hora" name="hora" @if ( isset($pedido) ) value="{{$pedido->hora}}" @else value="<?php $hora= date("h:m:s"); echo $hora; ?>" @endif required="required" class="form-control col-md-7 col-xs-12" title="Hora">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="total"></label>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<input type="text" id="num_doc" name="num_doc" disabled="disabled" @if ( isset($pedido) ) value="{{$pedido->num_doc}}" @endif class="form-control col-md-7 col-xs-12" title="Nº Documento" placeholder="Nº Documento">
								</div>
								<div class="col-md-3 col-sm-3 col-xs-12">
									<input type="text" id="total" name="total"  @if ( isset($pedido) ) value="{{$pedido->total}}" @else value="" @endif disabled="disabled" class="form-control col-md-7 col-xs-12" title="Total" placeholder="Total">
								</div>
							</div>
							<div class="form-group">      
								<label class="control-label col-md-1 col-sm-1 col-xs-12" for="hora"></label>
								<div class="col-md-10 col-sm-10 col-xs-12">    
									<div class="x_panel">                      
										<div class="x_content" >
											<div class="" role="tabpanel" data-example-id="togglable-tabs">
												<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
													<li role="presentation" class="active">
														<a href="#tab_Sets" id="detalles_tab" role="tab" data-toggle="tab" aria-expanded="true">Detalles</a>
													</li>
												</ul>
												<div id="myTabContent" class="tab-content">
													<div role="tabpanel" class="tab-pane fade active in" id="tab_Sets" aria-labelledby="detalles_tab">
														 @if ( isset($pedido) )
															<div class="row" id="botones_lineas">
																<div class="col-md-8">
																	<div class="btn-group">
																		<button class="btn btn-sm btn-default" onclick="nuevo();" title="Nuevo" type="button" id="btn_alt_linea" data-toggle="modal" data-target="#modal_linea">
																			<i class="glyphicon glyphicon-plus"></i>
																		</button>
																		<button class="btn btn-sm btn-default" title="Editar" type="button" id="btn_mod_linea" data-toggle="modal" data-target="">
																			<i class="glyphicon glyphicon-pencil"></i>
																		</button>
																		<button class="btn btn-sm btn-default" title="Eliminar" type="button" id="btn_baj_linea">
																			<i class="glyphicon glyphicon-remove"></i>
																		</button>
																	</div>
																</div>                           
															</div>
														@endif                                
														<div class="ln_solid"></div>
														<div id="tabla_lineas" style="max-height: 280px; overflow-y: scroll; overflow-x: hidden;">
															<table id="lineas_pedido" class="table table-striped table-bordered table-hover">
																<thead>
																	<tr>
																		<th style='text-align:center; width: 10px'><input type='checkbox' id='checkAllLineas' onchange='selectAllLineas(this);'></th>
																		<th style='text-align:center; width: 15px;'>#</th>
																		<th style='text-align:center;'>Articulo</th>
																		<th style='text-align:center; width: 150px;'>Cantidad</th>
																		<th style='text-align:center; width: 150px;'>Precio</th>
																		<th style='text-align:center; width: 150px;'>Importe</th>
																	</tr>
																</thead>
																<tbody>
																	@if ( isset($pedido) )
																		<?php $lineas = $pedido->lineas; ?>
																		<?php if ( sizeof( $lineas ) > 0 ): ?>
																			<?php $cont = 0; ?>
																			<?php foreach ($lineas as $item): ?>
																				<?php $cont = $cont + 1; ?>
																				<tr>
																					<th style='text-align:center;'>
																						<input type='checkbox' value='{{$item->id."*".$item->articulo_id."*".$item->cantidad."*".$item->precio."*".$item->importe}}'>
																					</th>
																					<td>{{$cont}}</td>
																					<td>{{$item->articulo->nombre}}</td>
																					<td>{{$item->cantidad}}</td>
																					<td>{{$item->precio}}</td>
																					<td>{{$item->importe}}</td>
																				</tr>												
																			<?php endforeach; ?>																			
																		<?php endif ?>
																	@endif
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> 
							</div>  
							<div class="form-group">
								<div class="pull-right col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
									<button type="submit" class="btn btn-success">Enviar</button>
									@if ( isset($pedido) )
										<a href="/pedidos" class="btn btn-default"> Culminar</a>
									@endif 
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
  <!-- Modal Lineas -->
  @if ( isset($pedido) )
  	@include('pedidos.modal_lineas')
  @endif
  <!-- /Modal -->
@endsection

@section('mas_scripts')
	@if ( isset($pedido) )
		<script type="text/javascript">
			$(document).ready(function() { 
				var select = $('#empresa_id');
			  cargar_articulos(select);
			});
			function cargar_articulos(select){
				var empresa_id = $(select).val();
				if ( empresa_id != ""){
					$("#botones_lineas").show();
					var data = $('#demo-form2').serialize();
					$.ajax({
				    url: "{{url('cargar_articulos_emp')}}",
				    type: "POST",      
				    data: data,
				    beforeSend: function(){
				      $("#articulos").html("<label for='ex3'>Buscando Artículos </label>");      
				    },
				    error: function(resp){
				      mensaje("Ha ocurrido un error al cargar los Artículos", 0);
				    },
				    success: function(resp){
				      if ( resp != 'Error' ){
				        $("#articulos").html("<label for='ex3'>Artículo </label>"+resp);     
				      }else{
				        mensaje("Ha ocurrido un error", 0);
				      }
				    }
				  });				
				}else{
					$("#articulos").html("<label for='ex3'>Artículos </label><input type='text' class='form-control' style='max-width: 723px;' value='Seleccione una empresa para el pedido' disabled='disabled'>");  
					$("#botones_lineas").hide();
				}
			}

			function cargar_precio(select){
				var articulo_id = $(select).val();
				if ( articulo_id != ""){
					var data = $('#form_linea_pedido').serialize();
					$.ajax({
				    url: "{{url('cargar_precio_art')}}",
				    type: "POST",      
				    data: data,
				    beforeSend: function(){
				      $("#precio").attr('placeholder', 'Buscando Precio');      
				    },
				    error: function(resp){
				      mensaje("Ha ocurrido un error al cargar el precio del Artículo", 0);
				    },
				    success: function(resp){
				      if ( resp != 'Error' ){				        
								$("#precio").val(resp);
								if ( $("#cantidad").val() != 0 ){
									var can = $("#cantidad").val(); 
									var pre = $("#precio").val();  
									$("#importe").val(can * pre);  
								}else{
									var can = $("#cantidad").val(); 
									var pre = $("#precio").val(); 
									$("#importe").val(can * pre);  
								}
				      }else{
				        mensaje("Ha ocurrido un error al cargar los datos del Artículo", 0);
				      }
				    }
				  });				
				}else{
					$("#precio").val(0);  
				  $("#precio").attr('placeholder', 'Precio'); 
					$("#importe").val(0);  
				  $("#precio").attr('placeholder', 'Importe'); 
				}
			}

			function calcular_importe(input){
				var can = $(input).val();
				if (can < 0) { can = 0; }
				var pre = $("#precio").val();  
				$("#importe").val(can * pre);
			}

			function selectAllLineas(checkbox){
			  if( checkbox.checked === true ) {
			    $("#lineas_pedido input:checkbox").each(function(){
			      $(this).prop('checked',true);            
			    });
			  }else{
			    $("#lineas_pedido input:checkbox").each(function(){
			      $(this).prop('checked',false);            
			    });
			  }
			};

			function nuevo(){
				$("#btn_reset").click();				
				$("#form_linea_pedido").attr('action', '{{url("Nueva_Linea_Pedido")}}');
			}

			$('#btn_mod_linea').click(function(){
				$('#btn_reset').click();
				if ($("#lineas_pedido input:checkbox:checked").length > 0){
					// Editamos el primer check que se encuentre seleccionado
					var primRegSel = 1; // Bandera o Semaforo
					var datos;
					$("#lineas_pedido input:checkbox:checked").each(function(){          
						if ($(this).attr("id") != 'checkAllLineas')
						{  
							if (primRegSel)
							{
								primRegSel = 0;
								//Optenemos los datos del seleccionado
								datos = $(this).val().split("*");
							};                       
						};
					});
					// Cargamos los demas datos
					$('#linea_id').val(datos[0]);
					$('#articulo_id').val(datos[1]).attr("selected", "true");
					$('#cantidad').val(datos[2]);
					$('#precio').val(datos[3]);
					$('#importe').val(datos[4]);
					$("#form_linea_pedido").attr('action', '{{url("Editar_Linea_Pedido")}}');
					$(this).attr("data-target", "#modal_linea");
				}else{
					mensaje("Selecciona al menos un registro de la lista", 0);
				};
			});

		  $('#btn_baj_linea').click(function(){    
		    // Eliminamos los check que se encuentren seleccionado
		    if ($("#lineas_pedido input:checkbox:checked").length > 0){
		    	var num_reg_sel = $("#lineas_pedido input:checkbox:checked").length - 1;
		    	var num_reg = 0;
		      // Preguntamos si se quieren eliminar
		      var confirm= alertify.confirm('¿Eliminar?','Seguro desea eliminar los registros seleccionados?',null,null).set('labels', {ok:'Confirmar', cancel:'Cancelar'}); 
		      //callbak al pulsar botón positivo
		      confirm.set('onok', function(){          
		        var data = $('#demo-form2').serialize();
		        $("#lineas_pedido input:checkbox:checked").each(function(){          
		          if ($(this).attr("id") != 'checkAllLineas'){ 
		          	num_reg = num_reg + 1;
		            //Optenemos los datos del seleccionado
		            var datos = $(this).val().split("*");
		            data = data+"&linea_id="+datos[0];
		            $.ajax({
		              url: "{{url('Eliminar_linea_pedido')}}",
		              type: "POST",      
		              data:data,
		              beforeSend: function(){
		                $("#lineas_pedido").html("<p align='center'><img src='{{asset('imagenes/35.gif')}}' /></p>");
		              },
		              error: function(){  
		                mensaje("Error petición ajax, Ocurrió un error al intentar eliminar uno de los registros", 0);
		              },
		              success: function(resp){    
		                mensaje("Registro eliminado con éxito", 1); 
		              }
		           })                  
		          };
		        });	
        		cargar_lineas();	   
		      });	      
		    }else{
		      mensaje("Selecciona al menos un registro de la Lista", 0);
		    };
		  });	

		  function cargar_lineas(){
				var data = $('#demo-form2').serialize();
				$.ajax({
					url: "{{url('/cargar_lineas_pedido')}}",
					type: "POST",      
					data: data,
					beforeSend: function(){
						$("#tabla_lineas").html("<p align='center' id='imgCarga'><img src='{{asset('imagenes/35.gif')}}' /></p>");
					},
					error: function(){ 
						mensaje("Ha ocurrido un error al cargar los detalles", 0);
						$("#imgCarga").remove();
						return;
					},
					success: function(resp){
						$("#tabla_lineas").html(resp);
					}
				});
			};

		</script>
	@endif
@endsection
