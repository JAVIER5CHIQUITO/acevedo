<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nominas extends Model
{
    //
     protected $table='nominas';
    
    protected $fillable = ['entidad_id','corte_desde','corte_hasta'];
}
