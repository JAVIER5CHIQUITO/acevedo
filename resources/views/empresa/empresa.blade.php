
@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

  <div class="col-md-6">
  	<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading panel-color" style="background-color:  #095f59; color: #ffffff;">
		 			<h3 class="panel-title">  
				 			Empresa
					</h3>
				</div>
				<div class="panel-body">
		 			<form method="POST" action="{{url('Guardar_Empresa')}}"  data-parsley-validate="true">
						{{ csrf_field() }}
					<!-- begin wizard step-1 -->
						<fieldset>
							<div class="row">
								<legend> Datos de la Epresa:</legend>
								<!-- begin col-4 -->
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-4">Nombre:</label>
											<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Por favor complete este campo"  @if( !is_null($empresa) ) value="{{$empresa->nombre}}" @endif required>
										</div>
									</div>
								<!-- end col-4 -->
							</div>
							<!--begin row -->
							<br>
							<div class="col-md-12 pull-right">
								<button  type="submit" class="pull-right btn-large btn btn-success"> Guardar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
