@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

 <div class="col-md-9">
	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading panel-color" style="background-color:  #095f59; color: #ffffff;">
				<h3 class="panel-title">
					 @if ( isset($compra) ) Editar @else Nuevo @endif Ajuste
				</h3>
			</div>
				<div class="panel-body">
					<form @if ( isset($compra) ) action="{{url('Editar_Ajuste_Entrada')}}" @else action="{{url('Nuevo_Ajuste_Entrada')}}" @endif method="POST" data-parsley-validate="true" class="form-horizontal" id="demo-form2">
						 {{ csrf_field() }}
						<!-- begin wizard step-1 -->
						<fieldset>
							<div class="corregir col-md-12" id="corregir"></div>
							<legend>Datos Del Ajuste:</legend> 
								<div class="form-group">
									<label for="num_doc" class="col-sm-2 control-label">Nº Documento: </label>
									<div class="col-sm-3 col-md-3">
									<input disabled="disabled" type="text" id="num_doc" name="num_doc" @if ( isset($compra) ) value="{{$compra->num_doc}}" @endif class="form-control" placeholder="Nº Documento">
									</div>
								</div>		
								<div class="form-group">
									<label for="fecha" class="col-sm-2 control-label">Fecha: </label>
									<div class="col-sm-3 col-md-3">
									<input type="date" id="fecha" name="fecha" @if ( isset($compra) ) value="{{$compra->fecha}}" @else value="<?php $fecha= date("Y-m-d"); echo $fecha; ?>" @endif class="form-control" title="Fecha">
									</div>
								</div>		
								<div class="form-group">
									<label for="hora" class="col-sm-2 control-label">Hora: </label>
									<div class="col-sm-3 col-md-3">
									<input type="time" id="hora" name="hora" @if ( isset($compra) ) value="{{$compra->hora}}" @else value="<?php $hora= date("H:m:s"); echo $hora; ?>" @endif class="form-control" placeholder="Hora">
									</div>
								</div>   
							<div class="box" id='divMovimientos'>
								<div class="box-header">
									<h3 class="box-title"> <i class="glyphicon glyphicon-shopping-cart"></i> Detalles</h3>
									@if ( isset($compra) )									
										<?php $movimientos = $compra->movimientos; ?>  
										<div class="row" id="botones_lineas">
											<div class="col-md-8">
												<div class="btn-group">
													<button class="btn btn-sm btn-default" onclick="nuevo();" title="Nuevo" type="button" id="btn_alt_linea" data-toggle="modal" data-target="#modal_linea">
														<i class="glyphicon glyphicon-plus"></i>
													</button>
													<button class="btn btn-sm btn-default" title="Editar" type="button" id="btn_mod_linea" data-toggle="modal" data-target="">
														<i class="glyphicon glyphicon-pencil"></i>
													</button>
													<button class="btn btn-sm btn-default" title="Eliminar" type="button" id="btn_baj_linea">
														<i class="glyphicon glyphicon-remove"></i>
													</button>
												</div>
											</div>                           
										</div>
										<hr>
									@endif  
								</div>
								<!-- /.box-header -->
								<div class="box-body no-padding">
									<div id='lstMovimientos'>
										<div style='max-height: 280px; overflow-y: scroll; overflow-x: hidden;'>
											<table class='table table-bordered' id='movimientos'>
												<thead>
													<tr>
														<th style='text-align:center; width: 10px'><input type='checkbox' id='checkAllLineas' onchange='selectAllLineas(this);'></th>
														<th style='width: 10px'>#</th>
														<th style='text-align:center; width: 160px;'>Articulo</th>
														<th style='text-align:center; width: 100px;'>Existencia</th>
														<th style='text-align:center; width: 100px;'>Cantidad</th>
														<th style='text-align:center; width: 120px;'>Precio</th>
														<th style='text-align:center; width: 120px;'>Importe</th>
													</tr> 
												</thead> 
												<tbody>
													@if ( isset($compra) )	
														<?php if ( sizeof( $movimientos ) > 0 ): ?>
															<?php $cont = 0; ?>
															@foreach ($movimientos as $item)
																<?php $cont = $cont + 1; ?>
																<tr>
																	<th style='text-align:center;'>
																		<input type='checkbox' value='{{$item->id."*".$item->articulo_id."*".$item->cantidad."*".$item->precio."*".$item->importe}}'>
																	</th>
																	<td>{{$cont}}</td>
																	<td>{{$item->articulo->nombre}}</td>
																	<td @if($item->articulo->existencia < $item->cantidad) style="text-align:center; color: red;" @else style="text-align:center;" @endif >{{$item->articulo->existencia}}</td>
																	<td style="text-align:center;">{{$item->cantidad}}</td>
																	<td style="text-align:center;">{{$item->precio}}</td>
																	<td style="text-align:center;">{{$item->importe}}</td>
																</tr>
															@endforeach
														<?php else: ?>
															<tr>
																<td colspan="7">
																	<div class="alert alert-info alert-dismissible">
																		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
																		<h4><i class="icon fa fa-info"></i> Aviso!</h4>
																		No hay ningun detalle registrado a la fecha para este ajuste.
																	</div> 
																</td>                                              
															</tr>														
														<?php endif ?>
													@endif  
												</tbody>              
											</table>                    
										</div>
									</div>
								</div>   
							</div>
							<div class="row">
								<div class="pull-right">
									<div class="form-group">
										<label for="total" class="col-sm-3 control-label">Total: </label>
										<div class="col-sm-8">
										<input disabled="disabled" type="text" style="text-align: right;" id="total" name="total" @if ( isset($compra) ) value="{{$compra->total}}" @endif class="form-control" placeholder="Total">
										</div>
									</div>									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 pull-right">
									<a href="{{url('ver_ajustes_entradas')}}" class="pull-right btn-large btn btn-default"> Cerrar</a>
									<button  type="submit" class="pull-right btn-large btn btn-success"> Guardar</button>						
								</div>
							</div>
						</fieldset>
						@if ( isset($compra) )
							<input type="hidden" name="id" id="id" class="hidden" value="{{$compra->id}}">
						@endif
					</form> 
				</div>
		</div>
	</div>
</div>
	<!-- Modal Lineas -->
	@if ( isset($compra) )
		@include('compras.modal_lineas')
	@endif
	<!-- /Modal -->
@endsection

@section('mas_scripts')
	@if ( isset($compra) )
		<script type="text/javascript">
			function cargar_precio(select){
				var articulo_id = $(select).val();
				if ( articulo_id != ""){
					var data = $('#form_linea_compra').serialize();
					$.ajax({
						url: "{{url('cargar_precio_art')}}",
						type: "POST",      
						data: data,
						beforeSend: function(){
							$("#precio").attr('placeholder', 'Buscando Precio');      
						},
						error: function(resp){
							mensaje("Ha ocurrido un error al cargar el precio del Artículo", 0);
						},
						success: function(resp){
							if ( resp != 'Error' ){				        
								$("#precio").val(resp);
								var can = $("#cantidad").val(); 
								var pre = $("#precio").val(); 
								$("#importe").val(can * pre);  								
							}else{
								mensaje("Ha ocurrido un error al cargar los datos del Artículo", 0);
							}
							$("#precio").attr('placeholder', 'Precio'); 
							$("#precio").attr('placeholder', 'Importe'); 
						}
					});				
				}else{
					$("#precio").val(0);  
					$("#precio").attr('placeholder', 'Precio'); 
					$("#importe").val(0);  
					$("#precio").attr('placeholder', 'Importe'); 
				}
			}

			function calcular_importe(){
				var can = $("#cantidad").val(); 
				if (can < 0) { can = 0; }
				var pre = $("#precio").val(); 
				$("#importe").val(can * pre); 
			}

			function selectAllLineas(checkbox){
				if( checkbox.checked === true ) {
					$("#movimientos input:checkbox").each(function(){
						$(this).prop('checked',true);            
					});
				}else{
					$("#movimientos input:checkbox").each(function(){
						$(this).prop('checked',false);            
					});
				}
			};

			function nuevo(){
				$("#btn_reset").click();				
				$("#form_linea_compra").attr('action', '{{url("Nueva_Linea_Compra")}}');
			}

			$('#btn_mod_linea').click(function(){
				$('#btn_reset').click();
				if ($("#movimientos input:checkbox:checked").length > 0){
					// Editamos el primer check que se encuentre seleccionado
					var primRegSel = 1; // Bandera o Semaforo
					var datos;
					$("#movimientos input:checkbox:checked").each(function(){          
						if ($(this).attr("id") != 'checkAllLineas')
						{  
							if (primRegSel)
							{
								primRegSel = 0;
								//Optenemos los datos del seleccionado
								datos = $(this).val().split("*");
							};                       
						};
					});
					// Cargamos los demas datos
					$('#linea_id').val(datos[0]);
					$('#articulo_id').val(datos[1]).attr("selected", "true");
					$('#cantidad').val(datos[2]);
					$('#precio').val(datos[3]);
					$('#importe').val(datos[4]);
					$("#form_linea_compra").attr('action', '{{url("Editar_Linea_Compra")}}');
					$(this).attr("data-target", "#modal_linea");
				}else{
					mensaje("Selecciona al menos un registro de la lista", 0);
				};
			});

			$('#btn_baj_linea').click(function(){    
				// Eliminamos los check que se encuentren seleccionado
				if ($("#movimientos input:checkbox:checked").length > 0){
					var num_reg_sel = $("#movimientos input:checkbox:checked").length - 1;
					var num_reg = 0;
					// Preguntamos si se quieren eliminar
					var confirm= alertify.confirm('¿Eliminar?','Seguro desea eliminar los registros seleccionados?',null,null).set('labels', {ok:'Confirmar', cancel:'Cancelar'}); 
					//callbak al pulsar botón positivo
					confirm.set('onok', function(){          
						var data = $('#demo-form2').serialize();
						$("#movimientos input:checkbox:checked").each(function(){          
							if ($(this).attr("id") != 'checkAllLineas'){ 
								num_reg = num_reg + 1;
								//Optenemos los datos del seleccionado
								var datos = $(this).val().split("*");
								data = data+"&linea_id="+datos[0];
								$.ajax({
									url: "{{url('Eliminar_linea_compra')}}",
									type: "POST",      
									data:data,
									beforeSend: function(){
										$("#movimientos").html("<p align='center'><img src='{{asset('imagenes/35.gif')}}' /></p>");
									},
									error: function(){  
										mensaje("Error petición ajax, Ocurrió un error al intentar eliminar uno de los registros", 0);
									},
									success: function(resp){    
										mensaje("Registro eliminado con éxito", 1); 
									}
							 })                  
							};
						});	
						cargar_lineas();	   
					});	      
				}else{
					mensaje("Selecciona al menos un registro de la Lista", 0);
				};
			});	

			function cargar_lineas(){
				var data = $('#demo-form2').serialize();
				$.ajax({
					url: "{{url('cargar_movimientos_compra')}}",
					type: "POST",      
					data: data,
					beforeSend: function(){
						$("#lstMovimientos").html("<p align='center' id='imgCarga'><img src='{{asset('imagenes/35.gif')}}' /></p>");
					},
					error: function(){ 
						mensaje("Ha ocurrido un error al cargar los detalles", 0);
						$("#imgCarga").remove();
						return;
					},
					success: function(resp){
						$("#lstMovimientos").html(resp);
					}
				});
			};

		</script>
	@endif
@endsection