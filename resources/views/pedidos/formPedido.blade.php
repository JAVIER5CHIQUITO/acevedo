@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

 <div class="col-md-9">
	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading panel-color" style="background-color:  #095f59; color: #ffffff;">
				<h3 class="panel-title">
					Confirmar Pedido
				</h3>
			</div>
				<div class="panel-body">
					<form action="{{url('Confirmar_Pedido')}}" method="POST" data-parsley-validate="true" class="form-horizontal">
						 {{ csrf_field() }}
						<!-- begin wizard step-1 -->
						<fieldset>
							<div class="corregir col-md-12" id="corregir"></div>
							<legend>Datos Del Pedido:</legend> 
								<div class="form-group">
								  <label for="empresa" class="col-sm-2 control-label">Empresa: </label>
								  <div class="col-sm-6">
									<input disabled="disabled" type="text" id="empresa" name="empresa" value="{{$pedido->empresa->nombre}}" class="form-control" placeholder="Empresa">
								  </div>
								</div>	
								<div class="form-group">
								  <label for="num_doc" class="col-sm-2 control-label">Nº Documento: </label>
								  <div class="col-sm-3 col-md-3">
									<input disabled="disabled" type="text" id="num_doc" name="num_doc" value="{{$pedido->num_doc}}" class="form-control" placeholder="Nº Documento">
								  </div>
								</div>	
								<div class="form-group">
								  <label for="cliente_id" class="col-sm-2 control-label">Cliente: </label>
								  <div class="col-sm-6">
									<input disabled="disabled" type="text" id="cliente_id" name="cliente_id" value="{{$pedido->cliente->nombre.' '.$pedido->cliente->apellido}}" class="form-control" placeholder="Cliente">
								  </div>
								</div>		
								<div class="form-group">
								  <label for="fecha" class="col-sm-2 control-label">Fecha: </label>
								  <div class="col-sm-3 col-md-3">
									<input disabled="disabled" type="text" style="text-align: right;" id="fecha" name="fecha" value="{{$pedido->fecha}}" class="form-control" placeholder="Fecha">
								  </div>
								</div>		
								<div class="form-group">
								  <label for="hora" class="col-sm-2 control-label">Hora: </label>
								  <div class="col-sm-3 col-md-3">
									<input disabled="disabled" type="text" style="text-align: right;" id="hora" name="hora" value="{{$pedido->hora}}" class="form-control" placeholder="Hora">
								  </div>
								</div>
							<?php $lineas = $pedido->lineas; ?>     
							<div class="box" id='divlineas'>
								<div class="box-header">
									<h3 class="box-title"> <i class="glyphicon glyphicon-shopping-cart"></i> Detalles</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body no-padding">
									<div id='lstLineas'>
										<div style='max-height: 280px; overflow-y: scroll; overflow-x: hidden;'>
											<table class="table table-bordered" id='lineas'>
												<thead>
													<tr>
														<th style='width: 10px'>#</th>
														<th style='text-align:center; width: 160px;'>Articulo</th>
														<th style='text-align:center; width: 100px;'>Existencia</th>
														<th style='text-align:center; width: 100px;'>Cantidad</th>
														<th style='text-align:center; width: 120px;'>Precio</th>
														<th style='text-align:center; width: 120px;'>Importe</th>
													</tr> 
												</thead> 
												<tbody>
													<?php if ( sizeof( $lineas ) > 0 ): ?>
														<?php $cont = 0; ?>
														@foreach ($lineas as $item)
															<?php $cont = $cont + 1; ?>
															<tr>
																<td>{{$cont}}</td>
																<td>{{$item->articulo->nombre}}</td>
																<td @if($item->articulo->existencia < $item->cantidad) style="text-align:center; color: red;"  @else style="text-align:center;" @endif>{{$item->articulo->existencia}}</td>
																<td style="text-align:center;">{{$item->cantidad}}</td>
																<td style="text-align:center;">{{$item->precio}}</td>
																<td style="text-align:center;">{{$item->importe}}</td>
															</tr>
														@endforeach
													<?php else: ?>
														<tr>
															<td colspan="6">
																<div class="alert alert-info alert-dismissible">
																	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
																	<h4><i class="icon fa fa-info"></i> Aviso!</h4>
																	No hay ningun detalle registrado a la fecha para este pedido.
																</div> 
															</td>                                              
														</tr>														
													<?php endif ?>
												</tbody>              
											</table>                    
										</div>
									</div>
								</div>   
							</div>
							<div class="row">
								<div class="pull-right">
									<div class="form-group">
									  <label for="total" class="col-sm-3 control-label">Total: </label>
									  <div class="col-sm-8">
										<input disabled="disabled" type="text" style="text-align: right;" id="total" name="total" value="{{$pedido->total}}" class="form-control" placeholder="Total">
									  </div>
									</div>									
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 pull-right">
									@if ($pedido->status == 0)
										<button  type="submit" class="pull-right btn-large btn btn-success"> Facturar</button>
									@else
										<a href="{{url('ver_pedidos')}}" class="pull-right btn-large btn btn-default"> Cerrar</a>
									@endif
								</div>
							</div>
						</fieldset>
						<input type="hidden" name="id" id="id" class="hidden" value="{{$pedido->id}}">
						<input type="hidden" name="empresa_id" id="empresa_id" class="hidden" value="{{$pedido->empresa_id}}">
					</form> 
				</div>
		</div>
	</div>
</div>
@endsection