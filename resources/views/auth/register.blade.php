@extends('layouts.header')

@section('content')

@include('layouts.panel-inicio')
  <div class="container">
     <div class="form-reg">
     <h3 class="panel-heading form-titulo">Registrate</h3>
      <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                               {{ csrf_field() }}
              <fieldset role="controlgroup">
                        <div class="form-group col-md-5{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="label label-warning" for="name">Nombre</label>
                               <input id="name" type="text" class="form-control" name="nombre" value="{{old('nombre')}}" required>

                                @if($errors->has('name'))
                                    <span class="help-block">
                                        <strong>nombre invalido o ya en uso</strong>
                                    </span>
                                @endif
                        </div>
                        
                        <div class="form-group col-md-offset-2 col-md-5{{ $errors->has('apellido') ? ' has-error' : '' }}">
                            <label class="label label-warning" for="apellido">Apelido</label>
                               <input id="apellido" type="text" class="form-control" name="apellido" value="" >

                                @if($errors->has('apellido'))
                                    <span class="help-block">
                                        <strong>Apellido invalido</strong>
                                    </span>
                                @endif
                        </div>
                        
                        <div class="form-group col-md-5{{ $errors->has('cedula') ? ' has-error' : '' }}">
                            <label class="label label-warning" for="cedula">Cedula</label>
                               <input id="cedula" type="text" class="form-control" name="cedula" value="" >

                                @if($errors->has('cedula'))
                                    <span class="help-block">
                                        <strong>Apellido invalido</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group col-lg-5{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="label label-warning" for="email">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{old('email')}}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>el correo ya en uso o es incorrecto</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group col-lg-5{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="label label-warning" for="password">contraseña</label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group col-lg-5">
                            <label class="label label-warning" for="password-confirm">Confirmar contraseña</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                        
                        <div class="form-group col-md-6">
                          <label class="label label-warning" for="tipo">Tipo</label>
                            <select name="tipo" id="tipo" class="form-control">
                              <option value="" disabled selected>....</option>
                              <option value="1">Empleado</option>
                              <option value="2">Proveedor</option>
                              <option value="3">Cliente</option>
                            </select>  
                        </div>
                <div class="col-md-10 col-sm-10 foot-form">
                  <button type="submit" class="btn btn-inverse">Registrar</button>
                </div>
              </fieldset>
               <hr>
            
        </form>
      </div>
            <br>
            <br>
            <br>

</div>
@endsection
