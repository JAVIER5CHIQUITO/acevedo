<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Ventas;
use App\Productos;
use App\Movimientos;
use App\Entidades;


class AjustesSalidasController extends Controller
{
    //
    public function index()
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
        if ( !is_null($empresa) ) {
            $facturas_ventas = $empresa->facturas_ventas->where('es_ajuste','=', 1);
            return view('ventas.ajustes', compact('facturas_ventas'));
        }else {
            return view('ventas.ajustes');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$venta = Ventas::find($id);
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$articulos = $empresa->articulos;
		return view('ventas.formAjuste', compact('articulos', 'venta'));
	}

	public function show_form()
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$articulos = $empresa->articulos;
		return view('ventas.formAjuste', compact('articulos'));
	}

	public function store(Request $request)
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$cliente = Entidades::find(\Auth::user()->entidad_id);

		if ( (!is_null($empresa)) && (!is_null($cliente)) ) {  

			$ult_ajuste = $empresa->facturas_ventas->where('es_ajuste','=', 1)->last();
			if ( is_null( $ult_ajuste ) ) {
				$num_doc = "AJS001";
			} else {
				$num_doc = substr( $ult_ajuste->num_doc, 5, strlen( $ult_ajuste->num_doc )) + 1;
				$num_doc = "AJS00".$num_doc;

			}

			$venta = new Ventas([
				'empresa_id'=>$empresa->id,
				'creador_id'=>\Auth::user()->entidad_id,
				'cliente_id'=>$cliente->id,
				'es_ajuste'=> 1,
				'num_doc'=> $num_doc,
				'fecha'=>$request['fecha'],
				'hora'=>$request['hora']
			]);  
			$venta->save(); 
			$mensaje = "Registro creado con éxito, incluye los articulos a ajustar";            
			\Session::flash('msgExito', $mensaje );
			return redirect('/Ajuste/Salida/Ver/'.$venta->id);
		}else {
			$mensaje = "Error al ubicar la ficha de la empresa o el cliente seleccionado";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}

	public function update(Request $request)
	{
		$cliente = Entidades::find(\Auth::user()->entidad_id);
		$venta = Ventas::find($request['id']);
		if ( (!is_null($cliente)) && (!is_null($venta) ) ) {  
			$venta->cliente_id = $cliente->id;
			$venta->fecha = $request['fecha'];
			$venta->hora = $request['hora'];
			$venta->save(); 
			$mensaje = "Registro actualizado con éxito, incluye los articulos a ajustar";            
			\Session::flash('msgExito', $mensaje );
			return back();
		}else {
			$mensaje = "Error al ubicar la ficha del cliente o el ajuste";          
			\Session::flash('msgError', $mensaje );
			return back();
		}
	}

	public function destroy($id)
	{
		$empresa = Empresa::where('entidad_id','=',\Auth::user()->entidad_id)->first();
		$venta = Ventas::where([
									['id', '=', $id],
									['empresa_id', '=', $empresa->id]
								])->first();
		if ( !is_null($venta) ) {
			Movimientos::where('factura_venta_id', '=', $venta->id)->delete();
			$venta->delete();
			$mensaje = "Registro eliminado con éxito";            
			\Session::flash('msgExito', $mensaje );
			return redirect('ver_ajustes_salidas');
		}else {
			$mensaje = "Error al ubicar la ficha del Ajsute";          
			\Session::flash('msgError', $mensaje );            
			return redirect('ver_ajustes_salidas');
		}
	}
}
