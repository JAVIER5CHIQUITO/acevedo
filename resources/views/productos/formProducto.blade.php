@extends('layouts.header')

@section('content')
 
@include('layouts.menu')
@include('layouts.panel-admin')

 <div class="col-md-9">
	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading panel-color" style="background-color:  #095f59; color: #ffffff;">
				<h3 class="panel-title">
					Editar Producto
				</h3>
			</div>
				<div class="panel-body">
					<form action="{{url('Editar_Articulo')}}" method="POST" data-parsley-validate="true">
						 {{ csrf_field() }}
						<!-- begin wizard step-1 -->
						<fieldset>
							<div class="corregir col-md-12" id="corregir"></div>
							<div class="row">    
								<legend>Datos Del Producto:</legend>                                          
								<!-- begin col-6 -->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Empresa: </label>
										<select name="empresa_id" id="empresa_id" class="form-control selectpicker" data-size="10" data-live-search="true" required="required">
											<option value="{{$empresa->id}}" selected> {{$empresa->nombre}} </option>   
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input type="checkbox" name="es_especialidad" id= es_especialidad" value='1' @if($articulo->es_especialidad == 1) checked="true" @endif ><b> Especialidad de la Empresa</b>
									</div>
								</div>
								<!-- end col-6 -->                                              
							</div>
							<div class="row">
							<!-- begin col-4 -->
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label col-md-4">Código: </label>
										<input type="text" id="codigo" name="codigo" value="{{$articulo->codigo}}" class="form-control" required />
									</div>
								</div>
							<!-- end col-4 -->
								<!-- begin col-4 -->
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label col-md-4">Nombre: </label>
										<input type="text" id="nombre" name="nombre" value="{{$articulo->nombre}}" class="form-control" required />
									</div>
								</div>
								<!-- end col-4 -->
							</div>

							<div class="row">
							<!--  begin col-4 -->
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label col-md-4">Existencia</label>
										<input type="text" style="text-align: right;" id="existencia" name="existencia" value="{{$articulo->existencia}}" class="form-control" disabled="disabled" />
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label col-md-4">Precio</label>
										<input type="text" style="text-align: right;" id="precio" name="precio" value="{{$articulo->precio}}" class="form-control" disabled="disabled" />
									</div>
								</div>
							<!-- end col-4 -->
							</div>   
														    
							<div class="box" id='divMovimientos'>
								<div class="box-header">
									<h3 class="box-title"> <i class="glyphicon glyphicon-move"></i> Movimientos</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body no-padding">
									<div id='lstMiembros'>
										<div style='max-height: 280px; overflow-y: scroll; overflow-x: hidden;'>
											<table class="table table-bordered" id='movimientos'>
												<thead>
													<tr>
														<th style='width: 10px'>#</th>
														<th style='text-align:center; width: 100px;'>Fecha</th>
														<th style='text-align:center; width: 100px;'>Hora</th>
														<th style='text-align:center; width: 160px;'>Tipo</th>
														<th style='text-align:center; width: 160px;'>Documento</th>
														<th style='text-align:center; width: 120px;'>Cantidad</th>
														<th style='text-align:center; width: 120px;'>Precio</th>
														<th style='text-align:center; width: 120px;'>Importe</th>
													</tr> 
												</thead> 
												<tbody>
													<?php if ( sizeof( $movimientos ) > 0 ): ?>
														<?php $cont = 0; ?>
														@foreach ($movimientos as $item)
															<?php $cont = $cont + 1; ?>
															<tr>
																<td>{{$cont}}</td>
																<td>{{$item->fecha}}</td>
																<td>{{$item->hora}}</td>
																@if ( $item->factura_compra_id == "" )
																	<td>@if ($item->factura_venta->es_ajuste == 1) Ajuste de Inventario (S) @else Venta @endif</td>
																	<td>{{$item->factura_venta->num_doc}}</td>
																@elseif( $item->factura_venta_id == "" )
																	<td> @if ($item->factura_compra->es_ajuste == 1) Ajuste de Inventario (E) @else Compra @endif </td>
																	<td>{{$item->factura_compra->num_doc}}</td>
																@endif
																<td>{{$item->cantidad}}</td>
																<td>{{$item->precio}}</td>
																<td>{{$item->importe}}</td>
															</tr>
														@endforeach
													<?php else: ?>
														<tr>
															<td colspan="8">
																<div class="alert alert-info alert-dismissible">
																	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
																	<h4><i class="icon fa fa-info"></i> Aviso!</h4>
																	No hay ningun movimiento registrado a la fecha para este articulo.
																</div> 
															</td>                                              
														</tr>														
													<?php endif ?>
												</tbody>              
											</table>                    
										</div>
									</div>
								</div>   
							</div>
							<!-- end row -->
							<br>
							<!--begin row -->
							<div class="row">
								<div class="col-md-12 pull-right">
									<button  type="submit" class="pull-right btn-large btn btn-success"> Enviar</button>
								</div>
							</div>
						</fieldset>
						<input type="hidden" name="id" id="id" class="hidden" value="{{$articulo->id}}">
					</form> 
				</div>
		</div>
	</div>
</div>
@endsection