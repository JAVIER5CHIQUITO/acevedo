<img src="imagenes/logsetu.jpeg" class="fondo">

<header>
  <div class="container clearfix">
    <h1 id="logo">
        Acevedo
    </h1>
   </div>
</header><!-- /header -->

<nav class="navbar navbar-m2p sidebar" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only-focusable">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                Embutidos Acevedo
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <!-- Dashboard -->
                <li class="">
                  <a href="{{url('historia')}}">
                    <span class="icono glyphicon glyphicon-lock pull-right xs-hidden"></span> Empresa
                  </a>
                </li>
                
                <li class="">
                  <a href="login">
                    <span class="icono glyphicon glyphicon-user pull-right xs-hidden"></span> Usuario
                  </a>
                </li>
                
                <li class="">
                  <a href="#">
                    <span class="icono glyphicon glyphicon-bell pull-right xs-hidden"></span> Especialidades
                  </a>
                </li>
                
                <li class="">
                  <a href="{{url('ubicacion')}}">
                    <span class="icono glyphicon glyphicon-road pull-right xs-hidden"></span> Ubicacion
                  </a>
                </li>
                
                <li class="">
                  <a href="{{url('pedidos')}}">
                    <span class="icono glyphicon glyphicon-shopping-cart pull-right xs-hidden"></span> Pedidos
                  </a>
                </li>
                
            </ul>
        </div>
    </div>
</nav>
