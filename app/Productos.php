<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    //
    protected $table='articulos';
    
    protected $fillable = ['empresa_id','codigo','nombre','precio','existencia'];

	public function movimientos(){
    	return $this->hasMany( Movimientos::class, 'articulo_id', 'id');
    }
    
}
