@extends('layouts.header')

@section('content')

@include('layouts.panel-inicio')
<section id="contact">
  <div class="container">
    <div class="well well-sm ub-map">
      <h3><strong>Ubiquenos</strong></h3>
    </div>
	
	<div class="row">
	  <div class="col-md-7">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3536.4960389883645!2d-48.513254785230025!3d-27.578146927925715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x952738f2e24ad5db%3A0x7ec0726139708001!2s4Effect+Tecnologia!5e0!3m2!1spt-BR!2sbd!4v1511541809921" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>      </div>

      <div class="col-md-5">
          <h4><strong>Get in Touch</strong></h4>
        <form>
          <div class="form-group">
            <input type="text" class="form-control" name="" value="" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="" value="" placeholder="E-mail">
          </div>
          <div class="form-group">
            <input type="tel" class="form-control" name="" value="" placeholder="Phone">
          </div>
          <div class="form-group">
            <textarea class="form-control" name="" rows="3" placeholder="Message"></textarea>
          </div>
          <button class="btn btn-default" type="submit" name="button">
              <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Submit
          </button>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection