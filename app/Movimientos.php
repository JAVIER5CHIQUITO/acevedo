<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimientos extends Model
{
    //
     protected $table='movimientos';
    
    protected $fillable = ['articulo_id','factura_compra_id','factura_venta_id','fecha','hora','cantidad','precio','importe'];

    public function factura_compra(){
    	return $this->belongsTo( Compras::class );
    }

    public function factura_venta(){
    	return $this->belongsTo( Ventas::class );
    }

    public function articulo(){
    	return $this->belongsTo( Productos::class );
    }
}
