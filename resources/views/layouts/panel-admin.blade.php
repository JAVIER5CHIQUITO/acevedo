<!-- Main -->
<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">

    <ul class="nav nav-pills nav-stacked" style="border-right:1px solid black">
        <!--<li class="nav-header"></li>-->
        <li> <a href="{{url('/dashboard')}}"><i class="glyphicon glyphicon-home"> </i> Dashboard</a></li>
        <li> <a href="{{url('ver_empresa')}}"> <i class="glyphicon glyphicon-bookmark"> </i> Empresa</a></li>
        <li> <a href="{{url('ver_productos')}}"> <i class="glyphicon glyphicon-barcode"> </i> Productos</a></li>
        <li> <a href="{{url('ver_pedidos')}}"><i class="glyphicon glyphicon-shopping-cart"></i> Pedidos</a></li>
        <li>
          <a id="panel10" href="javascript:;" data-toggle="collapse" data-target="#btc">
            <i class="glyphicon glyphicon-user"></i> Afiliados
            <i class="glyphicon glyphicon-menu-down pull-right" id="arow10"></i>
          </a>
          <ul class="collapse nav" id="btc">
            <li> <a href="{{url('aprobar_usuarios')}}"><i class="glyphicon "></i> Aprobar Usuarios</a> </li>
            <li> <a href="{{url('ver_clientes')}}"><i class="glyphicon "></i> Ver Clientes</a> </li>
            <li> <a href="{{url('ver_proveedores')}}"><i class="glyphicon "></i> Ver Proveedores</a> </li>
            <li> <a href="{{url('ver_empleados')}}"><i class="glyphicon "></i> Ver Empleados</a> </li>
          </ul>
        </li>
        <li><a href="{{url('ver_compras')}}"><i class="glyphicon glyphicon-piggy-bank"></i> Compras</a></li>
        <li><a href="{{url('ver_ventas')}}"><i class="glyphicon glyphicon-magnet"></i> Ventas</a></li>
        <li>
          <a id="panel11" href="javascript:;" data-toggle="collapse" data-target="#ajustes">
            <i class="glyphicon glyphicon-sort"></i> Ajustes de Inventario
            <i class="glyphicon glyphicon-menu-down pull-right" id="arow10"></i>
          </a>
          <ul class="collapse nav" id="ajustes">
            <li><a href="{{url('ver_ajustes_entradas')}}"><i class="glyphicon glyphicon-upload"></i> Ajustes de Entradas</a></li>
            <li><a href="{{url('ver_ajustes_salidas')}}"><i class="glyphicon glyphicon-download"></i> Ajustes de Salidas</a></li>
          </ul>
        </li>

    </ul>
</div><!-- /span-3 -->