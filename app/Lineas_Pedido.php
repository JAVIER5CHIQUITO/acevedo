<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lineas_Pedido extends Model
{
    //
    protected $table='pedidos_lineas';
    
    protected $fillable = ['articulo_id','pedido_venta_id','cantidad','precio','importe'];

    public function articulo(){
    	return $this->belongsTo( Productos::class );
    }
}
